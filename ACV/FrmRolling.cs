﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ACVWeightCapture
{
    public partial class FrmRolling : Form
    {
        private TextBox focusedTextbox = null;
        public FrmRolling()
        {
            InitializeComponent();
            touchScreen1.OnUserControlButtonClicked += new TouchScreen.ButtonClickedEventHandler(touchScreen1_OnUserControlButtonClicked);
            this.FormClosing += FrmRolling_FormClosing;
        }
        int YearId, MonthId, LastNo;
        int BUid = 0;
        SQLDBHelper db = new SQLDBHelper();
        private void FrmRolling_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    DialogResult result = MessageBox.Show("Do you really Close the form?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                    if (result == DialogResult.Yes)
                    {
                        this.Hide();
                        FrmQc qc = new FrmQc();
                        qc.Show();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void touchScreen1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                if (focusedTextbox != null)
                {
                    if (b.Text == "Back")
                    {
                        if (focusedTextbox.Text.Length > 1)
                        {
                            focusedTextbox.Text = focusedTextbox.Text.Substring(0, focusedTextbox.Text.Length - 1);
                        }
                        else
                        {
                            focusedTextbox.Text = string.Empty;
                        }
                    }
                    else if (b.Text == "Clear")
                    {
                        focusedTextbox.Text = string.Empty;
                        focusedTextbox.Focus();
                    }
                    else if (b.Text == "TAB")
                    {
                        this.SelectNextControl(focusedTextbox, true, true, true, true);
                    }
                    else if (b.Text == "Enter")
                    {
                        EnterKeyPress(focusedTextbox.Name);
                    }
                    else
                    {
                        if (MyGlobal.bTouch)
                            focusedTextbox.Text = b.Text;
                        else
                        {
                            MyGlobal.bTouch = false;
                            focusedTextbox.Text += b.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void EnterKeyPress(string Name)
        {
            try
            {
                if (Name == "txtMender")
                {
                    MendorKeyPress();
                }
                else if (Name == "txtbarcode")
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Barcode",txtbarcode.Text)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortNoRollNo", para);
                    lblSortNo.Text = dt.Rows[0]["SortNo"].ToString();
                    lblWtPMtr.Text = dt.Rows[0]["Pwpm"].ToString();
                    txtWeight.Tag = dt.Rows[0]["WTMtr"].ToString();
                    if (lblWtPMtr.Text == "0.000")
                    {
                        lblWtPMtr.Text = dt.Rows[0]["WtMtr"].ToString();
                    }
                    txtbarcode.Tag = dt.Rows[0]["id"].ToString();
                    //txtMeter.Text = dt.Rows[0]["PickLength"].ToString();
                    //if (txtMeter.Text == "0.000")
                    //{
                    //    txtMeter.Text = dt.Rows[0]["Lengn"].ToString();
                    //}
                    //txtWeight.Text = dt.Rows[0]["NetWt"].ToString();
                    txtMeter.Focus();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Do you really Close the form?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    this.Hide();
                    FrmQc qc = new FrmQc();
                    qc.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmRolling_Load(object sender, EventArgs e)
        {
            lblTitle.Width = this.Height - 100;
            grRolling.Height = this.Height - 70;
            grRolling.Width = this.Width - 20;
            panelMain.Location = new Point(ClientSize.Width / 2 - panelMain.Size.Width / 2, ClientSize.Height / 2 - panelMain.Size.Height / 2);
            panelMain.Anchor = AnchorStyles.Left;
            grFront.Height = this.Height - 70;
            grFront.Width = this.Width - 20;
            DataGridRollingFront.Height = grFront.Height - 60;
            DataGridRollingFront.Width = grFront.Width - 220;
            txtSearch.Width = DataGridRollingFront.Width;
            grRolling.Visible = false;
            grFront.Visible = true;
            LoadDataGridTitle();
            LoadFrontGrid();
            comboBox1.SelectedIndex = 0;

        }

        protected void LoadFrontGrid()
        {
            SqlParameter[] parameters = {
                new SqlParameter("@Date",Convert.ToDateTime(dtpfilterDate.Text).ToString("yyyy-MM-dd")),
                new SqlParameter("@QCSts",comboBox1.Text)
            };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetRolling", parameters);
            DataGridRollingFront.AutoGenerateColumns = false;
            DataGridRollingFront.DataSource = null;
            DataGridRollingFront.ColumnCount = 10;
            DataGridRollingFront.Columns[0].Name = "ID";
            DataGridRollingFront.Columns[0].HeaderText = "ID";
            DataGridRollingFront.Columns[0].DataPropertyName = "ID";
            DataGridRollingFront.Columns[0].Visible = false;

            DataGridRollingFront.Columns[1].Name = "Dte";
            DataGridRollingFront.Columns[1].HeaderText = "Date";
            DataGridRollingFront.Columns[1].DataPropertyName = "Dte";

            DataGridRollingFront.Columns[2].Name = "Time";
            DataGridRollingFront.Columns[2].HeaderText = "Time";
            DataGridRollingFront.Columns[2].DataPropertyName = "Tme";

            DataGridRollingFront.Columns[3].Name = "SortNo";
            DataGridRollingFront.Columns[3].HeaderText = "SortNo";
            DataGridRollingFront.Columns[3].DataPropertyName = "SortNo";
            DataGridRollingFront.Columns[3].Width = 180;

            DataGridRollingFront.Columns[4].Name = "RollBarcode";
            DataGridRollingFront.Columns[4].HeaderText = "Barcode";
            DataGridRollingFront.Columns[4].DataPropertyName = "RollBarcode";
            DataGridRollingFront.Columns[4].Width = 150;

            DataGridRollingFront.Columns[5].Name = "MendorName";
            DataGridRollingFront.Columns[5].HeaderText = "MendorName";
            DataGridRollingFront.Columns[5].DataPropertyName = "MendorName";
            DataGridRollingFront.Columns[5].Width = 150;

            DataGridRollingFront.Columns[6].Name = "TotalWeight";
            DataGridRollingFront.Columns[6].HeaderText = "Weight";
            DataGridRollingFront.Columns[6].DataPropertyName = "TotalWeight";
            DataGridRollingFront.Columns[6].Width = 150;
            DataGridRollingFront.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridRollingFront.Columns[6].DefaultCellStyle.Format = "N3";

            DataGridRollingFront.Columns[7].Name = "Meters";
            DataGridRollingFront.Columns[7].HeaderText = "Meters";
            DataGridRollingFront.Columns[7].DataPropertyName = "Meters";
            DataGridRollingFront.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridRollingFront.Columns[8].Name = "NoofPieces";
            DataGridRollingFront.Columns[8].HeaderText = "NoofPieces";
            DataGridRollingFront.Columns[8].DataPropertyName = "NoofPieces";
            DataGridRollingFront.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            DataGridRollingFront.Columns[9].Name = "Status";
            DataGridRollingFront.Columns[9].HeaderText = "Status";
            DataGridRollingFront.Columns[9].DataPropertyName = "QCSts";
            DataGridRollingFront.Columns[9].Width = 90;
            DataGridRollingFront.DataSource = dt;
        }


        protected void LoadDataGridTitle()
        {
            try
            {
                DataGridRolling.DataSource = null;
                DataGridRolling.AutoGenerateColumns = false;
                DataGridRolling.ColumnCount = 8;
                DataGridRolling.Columns[0].Name = "SlNo";
                DataGridRolling.Columns[0].HeaderText = "SlNo";

                DataGridRolling.Columns[1].Name = "Barcode";
                DataGridRolling.Columns[1].HeaderText = "Barcode";
                DataGridRolling.Columns[1].Width = 200;

                DataGridRolling.Columns[2].Name = "SortNo";
                DataGridRolling.Columns[2].HeaderText = "SortNo";
                DataGridRolling.Columns[2].Width = 180;
                DataGridRolling.Columns[2].Visible = false;

                DataGridRolling.Columns[3].Name = "Meter";
                DataGridRolling.Columns[3].HeaderText = "Meter";
                DataGridRolling.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridRolling.Columns[3].Width = 120;

                DataGridRolling.Columns[4].Name = "Weight";
                DataGridRolling.Columns[4].HeaderText = "Weight";
                DataGridRolling.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridRolling.Columns[4].Width = 120;

                DataGridRolling.Columns[5].Name = "WtMtr";
                DataGridRolling.Columns[5].HeaderText = "WtMtr";
                DataGridRolling.Columns[5].Visible = false;
                DataGridRolling.Columns[6].Name = "SortUid";
                DataGridRolling.Columns[6].HeaderText = "SortUid";
                DataGridRolling.Columns[6].Visible = false;
                DataGridRolling.Columns[7].Name = "Status";
                DataGridRolling.Columns[7].HeaderText = "Status";
                DataGridRolling.Columns[7].Width = 120;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void chckNumpad_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtbarcode_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
            if(txtMender.Text == string.Empty)
            {
                MessageBox.Show("Mendor code Should Not Empty");
                txtMender.Focus();
            }
        }

        private void txtMeter_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void txtMender_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    MendorKeyPress();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void MendorKeyPress()
        {
            try
            {
                SqlParameter[] para = {
                        new SqlParameter("@TypeM_Uid",28),
                        new SqlParameter("@Active",1),
                        new SqlParameter("@Code",txtMender.Text)
                    };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                lblMendorName.Text = dt.Rows[0]["f2"].ToString();
                txtMender.Tag = dt.Rows[0]["Uid"].ToString();
                txtbarcode.Focus();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void txtMender_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void btnBack_Click_1(object sender, EventArgs e)
        {
            grRolling.Visible = false;
            grFront.Visible = true;
        }

        private void btnRolling_Click(object sender, EventArgs e)
        {
            grRolling.Visible = true;
            grFront.Visible = false;
            cmbType.SelectedIndex = 0;
            btnRefersh_Click(sender, e);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmQc qc = new FrmQc();
            qc.Show();
        }

        private void txtbarcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Barcode",txtbarcode.Text)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortNoRollNo", para);
                    if(dt.Rows.Count == 0)
                    {
                        MessageBox.Show("invalid RollNo ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    lblSortNo.Text = dt.Rows[0]["SortNo"].ToString();
                    lblWtPMtr.Text = dt.Rows[0]["Pwpm"].ToString();
                    if (lblWtPMtr.Text == "")
                    {
                        lblWtPMtr.Text = dt.Rows[0]["WtMtr"].ToString();
                    }
                    txtbarcode.Tag = dt.Rows[0]["id"].ToString();
                    txtWeight.Tag = dt.Rows[0]["WTMtr"].ToString();
                    txtMeter.Tag = dt.Rows[0]["PickLength"].ToString();
                    if (txtMeter.Tag == null)
                    {
                        txtMeter.Tag = dt.Rows[0]["Lengn"].ToString();
                    }
                    txtWeight.Tag = dt.Rows[0]["NetWt"].ToString();
                    txtMeter.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtbarcode.Text != string.Empty)
                {
                    bool entryFound = false;
                    decimal val = 0;
                    foreach (DataGridViewRow row1 in DataGridRolling.Rows)
                    {
                        object val2 = row1.Cells[1].Value;
                        if (val2 != null && val2.ToString() == txtbarcode.Text)
                        {
                            for (int i = 0; i < DataGridRolling.Rows.Count - 1; i++)
                            {
                                object barcode = DataGridRolling.Rows[i].Cells[1].Value.ToString();
                                if (barcode == val2)
                                {
                                    if (val == 0)
                                    {
                                        val = Convert.ToDecimal(row1.Cells[4].Value.ToString());
                                    }
                                    else
                                    {
                                        val += Convert.ToDecimal(row1.Cells[4].Value.ToString());
                                    }
                                }
                            }
                            val += Convert.ToDecimal(txtWeight.Text);
                            if (val <= Convert.ToDecimal(txtWeight.Tag.ToString()) + 10)
                            {
                                entryFound = false;
                                break;
                            }
                            else
                            {
                                entryFound = true;
                                break;
                            }
                        }
                    }
                    if (!entryFound)
                    {
                        val = Convert.ToDecimal(txtWeight.Text);
                        decimal val2 = Convert.ToDecimal(txtWeight.Tag.ToString()) + 5;
                        if (val <= val2)//add 5 kg extra to allow the rolling
                        {
                            decimal Weight = 0;
                            if (txtNetWeight.Text == string.Empty)
                            {
                                txtNetWeight.Text = "0";
                            }
                            Weight = Convert.ToDecimal(txtNetWeight.Text);
                            decimal wtprmtr = Convert.ToDecimal(lblWtPMtr.Text);
                            decimal calcuwtmtr = ((Convert.ToDecimal(txtWeight.Text) / Convert.ToDecimal(txtMeter.Text)) * 1000);
                            //MessageBox.Show(calcuwtmtr.ToString());
                            if (calcuwtmtr <= wtprmtr - 10)
                            {
                                DialogResult result = MessageBox.Show("Weight Per Meter < " + calcuwtmtr.ToString("0.000") + " Do You want Continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                                if (result == DialogResult.Yes)
                                {

                                }
                                else
                                {
                                    txtMeter.Focus();
                                    return;
                                }
                            }
                            if (calcuwtmtr >= wtprmtr + 10)
                            {
                                DialogResult result = MessageBox.Show("Weight Per Meter > " + calcuwtmtr.ToString("0.000") + " Do You want Continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2); if (result == DialogResult.Yes)
                                    if (result == DialogResult.Yes)
                                    {

                                    }
                                    else
                                    {
                                        txtMeter.Focus();
                                        return;
                                    }
                            }
                            DataGridViewRow row = (DataGridViewRow)DataGridRolling.Rows[0].Clone();
                            row.Cells[0].Value = DataGridRolling.Rows.Count;
                            row.Cells[1].Value = txtbarcode.Text;
                            row.Cells[2].Value = lblSortNo.Text;
                            row.Cells[3].Value = txtMeter.Text;
                            row.Cells[4].Value = txtWeight.Text;
                            row.Cells[5].Value = lblWtPMtr.Text;
                            row.Cells[6].Value = txtbarcode.Tag;
                            row.Cells[7].Value = cmbType.Text;
                            DataGridRolling.Rows.Add(row);
                            if (cmbType.Text == "QCPass")
                            {
                                txtNetWeight.Text = (Weight + Convert.ToDecimal(txtWeight.Text)).ToString();
                            }
                            ClearControl();
                            txtMender.Focus();
                            cmbType.SelectedIndex = 0;
                        }
                        else
                        {
                            MessageBox.Show("Enter the correct Barcode,Weight and Meter", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtWeight.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter Barcode,Weight and Meter", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtbarcode.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            txtbarcode.Text = string.Empty;
            txtbarcode.Tag = string.Empty;
            txtMeter.Text = string.Empty;
            txtWeight.Text = string.Empty;
            lblWtPMtr.Text = string.Empty;
        }

        private void btnRefersh_Click(object sender, EventArgs e)
        {
            LoadFrontGrid();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Do you want to delete this record", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (result == DialogResult.Yes)
                {
                    int Index = DataGridRollingFront.SelectedCells[0].RowIndex;
                    SqlParameter[] para = {
                        new SqlParameter("@ID",DataGridRollingFront.Rows[Index].Cells[0].Value),
                        new SqlParameter("@Message",SqlDbType.NVarChar,50)
                    };
                    para[1].Direction = ParameterDirection.Output;
                    string Meaage = db.ExecuteQueryString(CommandType.StoredProcedure, "SP_RollDelete", para, 1);
                    if (Meaage == "Deleted")
                    {
                        MessageBox.Show("Deleted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadFrontGrid();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Already Packed", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtBarcode = db.GetData(CommandType.StoredProcedure, "SP_GetRollBarcode");
                if (DataGridRollingFront.SelectedRows.Count > 0)
                {
                    int Index = DataGridRollingFront.SelectedCells[0].RowIndex;
                    FileStream fs = new FileStream(Application.StartupPath + "\\QCBarcode.txt", FileMode.Truncate, FileAccess.Write);
                    fs.Close();
                    for (int i = 0; i < dtBarcode.Rows.Count; i++)
                    {
                        string Var = dtBarcode.Rows[i]["VarType"].ToString();
                        if (Var == "Static")
                        {
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", dtBarcode.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                        }
                        else
                        {
                            int LineNUmber = Convert.ToInt32(dtBarcode.Rows[i]["LineNumber"].ToString());
                            if (LineNUmber == 10)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridRollingFront.Rows[Index].Cells[4].Value + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 12)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridRollingFront.Rows[Index].Cells[4].Value + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 19)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridRollingFront.Rows[Index].Cells[3].Value + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 20)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridRollingFront.Rows[Index].Cells[7].Value).ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 21)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridRollingFront.Rows[Index].Cells[6].Value).ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 24)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDateTime(DataGridRollingFront.Rows[Index].Cells[1].Value.ToString()).ToString("dd.MM.yyyy") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 27)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridRollingFront.Rows[Index].Cells[5].Value.ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                        }
                    }
                    Process.Start(Application.StartupPath + "\\BPQc.bat");
                    Process.Start(Application.StartupPath + "\\BPQc.bat");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ClearControl();
            lblSortNo.Text = string.Empty;
            lblMendorName.Text = string.Empty;
            txtMender.Text = string.Empty;
            DataGridRolling.Rows.Clear();
            txtMender.Focus();
            txtNetWeight.Text = string.Empty;
        }

        private void grFront_Enter(object sender, EventArgs e)
        {

        }

        private void grRolling_Enter(object sender, EventArgs e)
        {

        }

        private void dtpfilterDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadFrontGrid();
                CellValueChanged();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void CellValueChanged()
        {
            try
            {
                foreach (DataGridViewRow row in DataGridRollingFront.Rows)
                {
                    if (row.Cells[9].Value.ToString() == "QCPass")
                    {
                        row.DefaultCellStyle.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        row.DefaultCellStyle.BackColor = Color.Yellow;
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void txtWeight_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int ShiftId = 0;
                DateTime dte = DateTime.Now.Date;
                TimeSpan tme = DateTime.Now.TimeOfDay;
                int t1 = tme.Hours;
                if (t1 >= 8 && t1 < 16)
                {
                    ShiftId = 984;
                }
                else if (t1 >= 16 && t1 <= 24)
                {
                    ShiftId = 985;
                }
                else if (t1 >= 0 && t1 < 8)
                {
                    ShiftId = 986;
                }
                SqlParameter[] para = { new SqlParameter("@BarType", "Rolling") };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_BarcodePrint", para);
                YearId = Convert.ToInt32(dt.Rows[0]["YerarId"].ToString());
                MonthId = Convert.ToInt32(dt.Rows[0]["MonthId"].ToString());
                LastNo = Convert.ToInt32(dt.Rows[0]["LastSNo"].ToString());
                BUid = Convert.ToInt32(dt.Rows[0]["Uid"].ToString());
                LastNo += 1;
                string yu = LastNo.ToString("D3");
                string Barcode = YearId.ToString() + MonthId.ToString("D2") + BUid + yu;

                if (DataGridRolling.Rows.Count > 1)
                {
                    SqlParameter[] paraRoll = {
                        new SqlParameter("@ID","0"),
                        new SqlParameter("@RollDate",dtpDate.Text),
                        new SqlParameter("@TotalWeight",Convert.ToDecimal(txtNetWeight.Text)),
                        new SqlParameter("@ShiftID",ShiftId),
                        new SqlParameter("@MendorName",lblMendorName.Text),
                        new SqlParameter("@RollBarcode",Barcode),
                        new SqlParameter("@RollId",SqlDbType.Int),
                        new SqlParameter("@SortNo",lblSortNo.Text),
                        new SqlParameter("@QCSts","QCPass")
                    };
                    paraRoll[6].Direction = ParameterDirection.Output;
                    int Uid = db.ExecuteQuery(CommandType.StoredProcedure, "SP_RollM", paraRoll, 6);

                    for (int i = 0; i < DataGridRolling.Rows.Count - 1; i++)
                    {
                        SqlParameter[] paraRollD = {
                            new SqlParameter("@RefUid",Uid),
                            new SqlParameter("@RefBarcode",DataGridRolling.Rows[i].Cells[1].Value.ToString()),
                            new SqlParameter("@Meters",DataGridRolling.Rows[i].Cells[3].Value.ToString()),
                            new SqlParameter("@Wght",DataGridRolling.Rows[i].Cells[4].Value.ToString()),
                            new SqlParameter("@SortNo",DataGridRolling.Rows[i].Cells[2].Value.ToString()),
                            new SqlParameter("@QCSts",DataGridRolling.Rows[i].Cells[7].Value.ToString())
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "SP_RollD", paraRollD);
                    }
                    //MessageBox.Show("Recaord has been saved successfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    printBarcode(Uid);
                    DataGridRolling.Rows.Clear();
                    txtNetWeight.Text = string.Empty;
                    txtMender.Text = string.Empty;
                    lblMendorName.Text = string.Empty;
                    lblSortNo.Text = string.Empty;
                    SqlParameter[] para2 = { new SqlParameter("@BarType", "Rolling"), new SqlParameter("@LastSNo", LastNo) };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateBarcode", para2);
                }
                else
                {
                    MessageBox.Show("No Data Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void printBarcode(int Id)
        {
            try
            {
                SqlParameter[] parameters = { new SqlParameter("@ID", Id) };
                DataTable dataTable = db.GetData(CommandType.StoredProcedure, "SP_GetRollingForPrint", parameters);
                DataTable dtBarcode = db.GetData(CommandType.StoredProcedure, "SP_GetRollBarcode");
                if (DataGridRollingFront.SelectedRows.Count > 0)
                {
                    int Index = DataGridRollingFront.SelectedCells[0].RowIndex;
                    FileStream fs = new FileStream(Application.StartupPath + "\\QCBarcode.txt", FileMode.Truncate, FileAccess.Write);
                    fs.Close();
                    for (int i = 0; i < dtBarcode.Rows.Count; i++)
                    {
                        string Var = dtBarcode.Rows[i]["VarType"].ToString();
                        if (Var == "Static")
                        {
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", dtBarcode.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                        }
                        else
                        {
                            int LineNUmber = Convert.ToInt32(dtBarcode.Rows[i]["LineNumber"].ToString());
                            if (LineNUmber == 10)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dataTable.Rows[0]["RollBarcode"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 12)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dataTable.Rows[0]["RollBarcode"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 19)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dataTable.Rows[0]["SortNo"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 20)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(dataTable.Rows[0]["Meters"].ToString()).ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 21)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(dataTable.Rows[0]["TotalWeight"].ToString()).ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 24)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDateTime(dataTable.Rows[0]["Dte"].ToString()).ToString("dd.MM.yyyy") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 27)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dataTable.Rows[0]["MendorName"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                        }
                    }
                    Process.Start(Application.StartupPath + "\\BPQc.bat");
                    Process.Start(Application.StartupPath + "\\BPQc.bat");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
