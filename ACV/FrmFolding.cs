﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACVWeightCapture
{
    public partial class FrmFolding : Form
    {
        private TextBox focusedTextbox = null;
        public FrmFolding()
        {
            InitializeComponent();
            this.FormClosing += FrmFolding_FormClosing;
            touchScreen1.OnUserControlButtonClicked += new TouchScreen.ButtonClickedEventHandler(touchScreen1_OnUserControlButtonClicked);
        }
        SQLDBHelper db = new SQLDBHelper();
        int QcType;
        int YearId, MonthId, LastNo;
        int BUid = 0;
        private void touchScreen1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                if (focusedTextbox != null)
                {
                    if (b.Text == "Back")
                    {
                        if (focusedTextbox.Text.Length > 1)
                        {
                            focusedTextbox.Text = focusedTextbox.Text.Substring(0, focusedTextbox.Text.Length - 1);
                        }
                        else
                        {
                            focusedTextbox.Text = string.Empty;
                        }
                    }
                    else if (b.Text == "Clear")
                    {
                        focusedTextbox.Text = string.Empty;
                        focusedTextbox.Focus();
                    }
                    else if (b.Text == "Enter")
                    {
                        this.SelectNextControl(focusedTextbox, true, true, true, true);
                    }
                    else
                    {
                        if (MyGlobal.bTouch)
                            focusedTextbox.Text = b.Text;
                        else
                        {
                            MyGlobal.bTouch = false;
                            focusedTextbox.Text += b.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmFolding_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    DialogResult result = MessageBox.Show("Do you really Close the form?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                    if (result == DialogResult.Yes)
                    {
                        this.Hide();
                        FrmQc qc = new FrmQc();
                        qc.Show();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmFolding_Load(object sender, EventArgs e)
        {
            QcType = 1;
            LoadDatatable();
            grFolding.Width = this.Width - 20;
            grFolding.Height = this.Height - 70;
            panelMain.Location = new Point(ClientSize.Width / 2 - panelMain.Size.Width / 2, ClientSize.Height / 2 - panelMain.Size.Height / 2);
            panelMain.Anchor = AnchorStyles.None;
            txtbarcode.Focus();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Do you really Close the form?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    this.Hide();
                    FrmQc qc = new FrmQc();
                    qc.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void chckNumpad_CheckedChanged(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtbarcode_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void txtMeter_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void txtWeight_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void touchScreen1_Load(object sender, EventArgs e)
        {
           
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                txtbarcode.Text = string.Empty;
                txtMeter.Text = string.Empty;
                txtWeight.Text = string.Empty;
                txtbarcode.Focus();
                LoadDatatable();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] paraBarcode = { new SqlParameter("@BarType", "Folding") };
                DataTable dtBarcode = db.GetData(CommandType.StoredProcedure, "SP_BarcodePrint", paraBarcode);
                YearId = Convert.ToInt32(dtBarcode.Rows[0]["YerarId"].ToString());
                MonthId = Convert.ToInt32(dtBarcode.Rows[0]["MonthId"].ToString());
                LastNo = Convert.ToInt32(dtBarcode.Rows[0]["LastSNo"].ToString());
                BUid = Convert.ToInt32(dtBarcode.Rows[0]["Uid"].ToString());
                LastNo += 1;
                string yu = LastNo.ToString("D3");
                string Barcode = YearId.ToString() + MonthId.ToString("D2") + BUid + yu;
                SqlParameter[] para = { new SqlParameter("@Barcode", txtbarcode.Text) };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortForQC", para);
                int SortUid = Convert.ToInt32(dt.Rows[0]["ID"].ToString());
                SqlParameter[] paraQc = {
                    new SqlParameter("@RefUid",SortUid),
                    new SqlParameter("@RefBarcode",txtbarcode.Text),
                    new SqlParameter("@QcType",QcType),
                    new SqlParameter("@BarcodeQc",Barcode),
                    new SqlParameter("@Meter",Convert.ToDecimal(txtMeter.Text)),
                    new SqlParameter("@Wght",Convert.ToDecimal(txtWeight.Text)),
                    new SqlParameter("@QcDate",Convert.ToDateTime(dtpDate.Text)),
                    new SqlParameter("@ReturnUid",SqlDbType.Int)
                };
                paraQc[7].Direction = ParameterDirection.Output;
                db.ExecuteQuery(CommandType.StoredProcedure, "SP_QC", paraQc,7);
                SqlParameter[] para2 = { new SqlParameter("@BarType", "Folding"), new SqlParameter("@LastSNo", LastNo) };
                db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateBarcode", para2);
                txtbarcode.Text = string.Empty;
                txtMeter.Text = string.Empty;
                txtWeight.Text = string.Empty;
                txtbarcode.Focus();
                LoadDatatable();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadDatatable()
        {
            try
            {
                SqlParameter[] para = {
                    new SqlParameter("@QcType",QcType),
                    new SqlParameter("@Date",Convert.ToDateTime(dtpDate.Text).ToString("yyyy-MM-dd"))
                };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetQCData", para);
                DataGridFolding.DataSource = null;
                DataGridFolding.AutoGenerateColumns = false;
                DataGridFolding.ColumnCount = 9;
                DataGridFolding.Columns[0].Name = "Slno";
                DataGridFolding.Columns[0].HeaderText = "Slno";
                DataGridFolding.Columns[0].Width = 80;
                DataGridFolding.Columns[1].Name = "QCUid";
                DataGridFolding.Columns[1].HeaderText = "QCUid";
                DataGridFolding.Columns[1].DataPropertyName = "QCUid";
                DataGridFolding.Columns[1].Visible = false;
                DataGridFolding.Columns[2].Name = "RefUid";
                DataGridFolding.Columns[2].HeaderText = "RefUid";
                DataGridFolding.Columns[2].DataPropertyName = "RefUid";
                DataGridFolding.Columns[2].Visible = false;
                DataGridFolding.Columns[3].Name = "RefBarcode";
                DataGridFolding.Columns[3].HeaderText = "RefBarcode";
                DataGridFolding.Columns[3].DataPropertyName = "RefBarcode";
                DataGridFolding.Columns[3].Width = 140;
                DataGridFolding.Columns[4].Name = "QcType";
                DataGridFolding.Columns[4].HeaderText = "QcType";
                DataGridFolding.Columns[4].DataPropertyName = "QcType";
                DataGridFolding.Columns[4].Visible = false;
                DataGridFolding.Columns[5].Name = "BarcodeQc";
                DataGridFolding.Columns[5].HeaderText = "BarcodeQc";
                DataGridFolding.Columns[5].DataPropertyName = "BarcodeQc";
                DataGridFolding.Columns[5].Width = 140;
                DataGridFolding.Columns[6].Name = "Meter";
                DataGridFolding.Columns[6].HeaderText = "Meter";
                DataGridFolding.Columns[6].DataPropertyName = "Meter";
                DataGridFolding.Columns[6].DefaultCellStyle.Format = "N3";
                DataGridFolding.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridFolding.Columns[7].Name = "Weight";
                DataGridFolding.Columns[7].HeaderText = "Weight";
                DataGridFolding.Columns[7].DataPropertyName = "Wght";
                DataGridFolding.Columns[7].DefaultCellStyle.Format = "N3";
                DataGridFolding.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridFolding.Columns[8].Name = "QcDate";
                DataGridFolding.Columns[8].HeaderText = "QcDate";
                DataGridFolding.Columns[8].DataPropertyName = "QcDate";
                DataGridFolding.Columns[8].Width = 120;
                DataGridFolding.DataSource = dt;
                for (int i = 0; i < DataGridFolding.Rows.Count; i++)
                {
                    DataGridFolding.Rows[i].Cells[0].Value = i + 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
