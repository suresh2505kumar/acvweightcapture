﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace ACVWeightCapture
{
    public partial class FrmWarbBooking : Form
    {
        public FrmWarbBooking()
        {
            InitializeComponent();
            this.FormClosing += FrmWarbBooking_FormClosing;
        }

        private void FrmWarbBooking_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                FrmIOSource io = new FrmIOSource();
                io.Show();
            }
            else
            {
                e.Cancel = true;
            }
        }

        int Fillid = 0;
        int SelectId = 0;
        bool EditMode = false;
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsItem = new BindingSource();
        BindingSource bsBeam = new BindingSource();
        int BeamWeight = 0;
        int YearId, MonthId, LastNo;
        private void FrmWarbBooking_Load(object sender, EventArgs e)
        {
            getLocation();
            GetWarbBooking();
            SqlParameter[] para = { new SqlParameter("@BarType", "WarpBooking") };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_BarcodePrint", para);
            YearId = Convert.ToInt32(dt.Rows[0]["YerarId"].ToString());
            MonthId = Convert.ToInt32(dt.Rows[0]["MonthId"].ToString());
            LastNo = Convert.ToInt32(dt.Rows[0]["LastSNo"].ToString());
            btnSearch_Click(sender, e);
        }

        private void txtItem_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = getItem();
                FillGrid(dt, 2);
                Point loc = Genclass.FindLocation(txtItem);
                grSearch.Location = new Point(loc.X, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Item Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void getLocation()
        {
            try
            {
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetLocatoin");
                cmbLocation.DataSource = null;
                cmbLocation.DisplayMember = "Location";
                cmbLocation.ValueMember = "Uid";
                cmbLocation.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void GetWarbBooking()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Tag", "Select") };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetWarbBooking", para);
                LoadGetWarbBooking(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void LoadGetWarbBooking(DataTable dt)
        {
            try
            {
                DataGridWarbBooking.DataSource = null;
                DataGridWarbBooking.AutoGenerateColumns = false;
                DataGridWarbBooking.ColumnCount = 15;
                DataGridWarbBooking.Columns[0].Name = "WBUid";
                DataGridWarbBooking.Columns[0].HeaderText = "WBUid";
                DataGridWarbBooking.Columns[0].DataPropertyName = "WBUid";
                DataGridWarbBooking.Columns[0].Visible = false;

                DataGridWarbBooking.Columns[1].Name = "ItemUid";
                DataGridWarbBooking.Columns[1].HeaderText = "ItemUid";
                DataGridWarbBooking.Columns[1].DataPropertyName = "ItemUid";
                DataGridWarbBooking.Columns[1].Visible = false;

                DataGridWarbBooking.Columns[2].Name = "BeamUid";
                DataGridWarbBooking.Columns[2].HeaderText = "BeamUid";
                DataGridWarbBooking.Columns[2].DataPropertyName = "BeamUid";
                DataGridWarbBooking.Columns[2].Visible = false;

                DataGridWarbBooking.Columns[3].Name = "Date";
                DataGridWarbBooking.Columns[3].HeaderText = "Date";
                DataGridWarbBooking.Columns[3].DataPropertyName = "DocDate";
                DataGridWarbBooking.Columns[3].Width = 90;

                DataGridWarbBooking.Columns[4].Name = "ItemName";
                DataGridWarbBooking.Columns[4].HeaderText = "ItemName";
                DataGridWarbBooking.Columns[4].DataPropertyName = "ItemName";
                DataGridWarbBooking.Columns[4].Width = 150;

                DataGridWarbBooking.Columns[5].Name = "Beam";
                DataGridWarbBooking.Columns[5].HeaderText = "Beam";
                DataGridWarbBooking.Columns[5].DataPropertyName = "Beam";
                DataGridWarbBooking.Columns[5].Width = 70;

                DataGridWarbBooking.Columns[6].Name = "SetNo";
                DataGridWarbBooking.Columns[6].HeaderText = "SetNo";
                DataGridWarbBooking.Columns[6].DataPropertyName = "SetNo";
                DataGridWarbBooking.Columns[6].Width = 70;

                DataGridWarbBooking.Columns[7].Name = "Ends";
                DataGridWarbBooking.Columns[7].HeaderText = "Ends";
                DataGridWarbBooking.Columns[7].DataPropertyName = "Ends";
                DataGridWarbBooking.Columns[7].Width = 70;

                DataGridWarbBooking.Columns[8].Name = "Length";
                DataGridWarbBooking.Columns[8].HeaderText = "Length";
                DataGridWarbBooking.Columns[8].DataPropertyName = "WbLength";
                DataGridWarbBooking.Columns[8].Width = 70;

                DataGridWarbBooking.Columns[9].Name = "Gross";
                DataGridWarbBooking.Columns[9].HeaderText = "Gross";
                DataGridWarbBooking.Columns[9].DataPropertyName = "WBWeight";
                DataGridWarbBooking.Columns[9].DefaultCellStyle.Format = "N4";
                DataGridWarbBooking.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridWarbBooking.Columns[9].Width = 70;

                DataGridWarbBooking.Columns[10].Name = "BeamWeight";
                DataGridWarbBooking.Columns[10].HeaderText = "Tare Wght";
                DataGridWarbBooking.Columns[10].DataPropertyName = "BeamWeight";
                DataGridWarbBooking.Columns[10].Width = 70;

                DataGridWarbBooking.Columns[11].Name = "Weight";
                DataGridWarbBooking.Columns[11].HeaderText = "Net Weight";
                DataGridWarbBooking.Columns[11].DataPropertyName = "Net";
                DataGridWarbBooking.Columns[11].DefaultCellStyle.Format = "N4";
                DataGridWarbBooking.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridWarbBooking.Columns[11].Width = 70;

                DataGridWarbBooking.Columns[12].Name = "Location";
                DataGridWarbBooking.Columns[12].HeaderText = "Location";
                DataGridWarbBooking.Columns[12].DataPropertyName = "Location";
                DataGridWarbBooking.Columns[12].Visible = false;
                DataGridWarbBooking.Columns[12].Width = 70;

                DataGridWarbBooking.Columns[13].Name = "ActualWeight";
                DataGridWarbBooking.Columns[13].HeaderText = "Actual Wght";
                DataGridWarbBooking.Columns[13].DataPropertyName = "ActualWght";
                DataGridWarbBooking.Columns[13].DefaultCellStyle.Format = "N4";
                DataGridWarbBooking.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridWarbBooking.Columns[13].Width = 70;
            
                DataGridWarbBooking.Columns[14].Name = "Diffrence";
                DataGridWarbBooking.Columns[14].HeaderText = "Diffrence";
                DataGridWarbBooking.Columns[14].DataPropertyName = "Diff";
                DataGridWarbBooking.Columns[14].DefaultCellStyle.Format = "N4";
                DataGridWarbBooking.Columns[14].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridWarbBooking.Columns[14].Width = 70;
                DataGridWarbBooking.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FillGrid(DataTable dt, int FillId)
        {
            try
            {
                DataGridCommon.DataSource = null;
                DataGridCommon.AutoGenerateColumns = false;
                if (FillId == 2)
                {
                    Fillid = 2;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "ItemName";
                    DataGridCommon.Columns[2].Name = "ShortName";
                    DataGridCommon.Columns[2].HeaderText = "ShortName";
                    DataGridCommon.Columns[2].DataPropertyName = "ItemCode";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.Columns[0].Visible = false;
                    DataGridCommon.DataSource = bsItem;
                }
                else if (FillId == 3)
                {
                    Fillid = 3;
                    DataGridCommon.ColumnCount = 3;
                    DataGridCommon.Columns[0].Name = "Uid";
                    DataGridCommon.Columns[0].HeaderText = "Uid";
                    DataGridCommon.Columns[0].DataPropertyName = "Uid";
                    DataGridCommon.Columns[1].Name = "Name";
                    DataGridCommon.Columns[1].HeaderText = "Name";
                    DataGridCommon.Columns[1].DataPropertyName = "GeneralName";
                    DataGridCommon.Columns[2].Name = "Weight";
                    DataGridCommon.Columns[2].HeaderText = "Weight";
                    DataGridCommon.Columns[2].DataPropertyName = "BWght";
                    DataGridCommon.Columns[1].Width = 250;
                    DataGridCommon.DataSource = bsBeam;
                    DataGridCommon.Columns[0].Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private DataTable getBeam()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetData(CommandType.StoredProcedure, "SP_GetBeamNo");
                bsBeam.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }
        protected DataTable getItem()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = db.GetData(CommandType.StoredProcedure, "SP_GetItemNew");
                //dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                bsItem.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return dt;
        }

        private void txtBeam_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                DataTable dt = getBeam();
                FillGrid(dt, 3);
                Point loc = Genclass.FindLocation(txtBeam);
                grSearch.Location = new Point(loc.X - 60, loc.Y + 20);
                grSearch.Visible = true;
                grSearch.Text = "Beam Search";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCommon_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 2)
                {
                    txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtBeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtBeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    BeamWeight = Convert.ToInt32(DataGridCommon.Rows[Index].Cells[0].Value.ToString());
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtItem_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsItem.Filter = string.Format("ItemName LIKE '%{0}%' or ItemCode LIKE '%{1}%' ", txtItem.Text, txtItem.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtBeam_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (SelectId == 0)
                {
                    bsBeam.Filter = string.Format("GeneralName LIKE '%{0}%' ", txtBeam.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if(cmbLocation.SelectedIndex == -1)
                {
                    MessageBox.Show("Select Location", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbLocation.Focus();
                    return;
                }
                SqlParameter[] para1 = { new SqlParameter("@BarType", "WarpBooking") };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_BarcodePrint", para1);
                YearId = Convert.ToInt32(dt.Rows[0]["YerarId"].ToString());
                MonthId = Convert.ToInt32(dt.Rows[0]["MonthId"].ToString());
                LastNo = Convert.ToInt32(dt.Rows[0]["LastSNo"].ToString());
                int Uid = Convert.ToInt32(dt.Rows[0]["uID"].ToString());
                if (txtItem.Text != string.Empty || txtBeam.Text != string.Empty || txtSetno.Text != string.Empty || txtEnds.Text != string.Empty || txtlength.Text != string.Empty)
                {
                    LastNo += 1;
                    string Barcode = YearId.ToString() + MonthId.ToString("D2") + Uid + LastNo.ToString("D3");
                    if (EditMode == false)
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@ItemUid",txtItem.Tag),
                            new SqlParameter("@BeamUid",txtBeam.Tag),
                            new SqlParameter("@SetNo",txtSetno.Text),
                            new SqlParameter("@Ends",txtEnds.Text),
                            new SqlParameter("@WBLength",txtlength.Text),
                            new SqlParameter("@WBWeight",txtWeight.Text),
                            new SqlParameter("@DocDate",Convert.ToDateTime(dtpgrndt.Text)),
                            new SqlParameter("@Location",cmbLocation.Text),
                            new SqlParameter("@Barcode",Barcode),
                            new SqlParameter("@ActualWght","0.000")
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "SP_WarbBooking", para);
                    }
                    else
                    {
                        SqlParameter[] para = {
                            new SqlParameter("@ItemUid",txtItem.Tag),
                            new SqlParameter("@BeamUid",txtBeam.Tag),
                            new SqlParameter("@SetNo",txtSetno.Text),
                            new SqlParameter("@Ends",txtEnds.Text),
                            new SqlParameter("@WBLength",txtlength.Text),
                            new SqlParameter("@WBWeight",txtWeight.Text),
                            new SqlParameter("@DocDate",Convert.ToDateTime(dtpgrndt.Text)),
                            new SqlParameter("@Location",cmbLocation.Text),
                            new SqlParameter("@WBUid",txtSetno.Tag),
                            new SqlParameter("@Barcode",Barcode),
                            new SqlParameter("@ActualWght","0.000")
                        };
                        db.ExecuteQuery(CommandType.StoredProcedure, "SP_WarbBooking", para);
                        EditMode = false;
                    }
                    MessageBox.Show("Record has been saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                GetWarbBooking();
                SqlParameter[] para2 = { new SqlParameter("@BarType", "WarpBooking"), new SqlParameter("@LastSNo", LastNo) };
                db.GetData(CommandType.StoredProcedure, "SP_UpdateBarcode", para2);
                txtItem.Text = string.Empty;
                txtItem.Tag = string.Empty;
                txtBeam.Text = string.Empty;
                txtBeam.Tag = string.Empty;
                txtSetno.Text = string.Empty;
                txtSetno.Tag = string.Empty;
                txtlength.Text = string.Empty;
                txtEnds.Text = string.Empty;
                txtWeight.Text = string.Empty;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridWarbBooking_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F2)
                {
                    int Index = DataGridWarbBooking.SelectedCells[0].RowIndex;
                    txtItem.Text = DataGridWarbBooking.Rows[Index].Cells[4].Value.ToString();
                    txtItem.Tag = DataGridWarbBooking.Rows[Index].Cells[1].Value.ToString();
                    txtBeam.Text = DataGridWarbBooking.Rows[Index].Cells[5].Value.ToString();
                    txtBeam.Tag = DataGridWarbBooking.Rows[Index].Cells[2].Value.ToString();
                    txtSetno.Text = DataGridWarbBooking.Rows[Index].Cells[6].Value.ToString();
                    txtSetno.Tag = DataGridWarbBooking.Rows[Index].Cells[0].Value.ToString();
                    txtlength.Text = DataGridWarbBooking.Rows[Index].Cells[8].Value.ToString();
                    txtEnds.Text = DataGridWarbBooking.Rows[Index].Cells[7].Value.ToString();
                    txtWeight.Text = DataGridWarbBooking.Rows[Index].Cells[9].Value.ToString();
                    dtpgrndt.Text = DataGridWarbBooking.Rows[Index].Cells[3].Value.ToString();
                    cmbLocation.Text = DataGridWarbBooking.Rows[Index].Cells[12].Value.ToString();
                    DataGridWarbBooking.Rows.Remove(DataGridWarbBooking.Rows[Index]);
                    DataGridWarbBooking.ClearSelection();
                    EditMode = true;
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    int Index = DataGridWarbBooking.SelectedCells[0].RowIndex;
                    int WBUid = Convert.ToInt32(DataGridWarbBooking.Rows[Index].Cells[0].Value.ToString());
                    SqlParameter[] para = { new SqlParameter("@WBUid", WBUid) };
                    db.ExecuteQuery(CommandType.StoredProcedure, "SP_DeleteWarbBooking", para);
                    MessageBox.Show("Record has been deleted", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GetWarbBooking();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridWarbBooking_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridWarbBooking.SelectedCells[0].RowIndex;
                txtItem.Text = DataGridWarbBooking.Rows[Index].Cells[4].Value.ToString();
                txtItem.Tag = DataGridWarbBooking.Rows[Index].Cells[1].Value.ToString();
                txtBeam.Text = DataGridWarbBooking.Rows[Index].Cells[5].Value.ToString();
                txtBeam.Tag = DataGridWarbBooking.Rows[Index].Cells[2].Value.ToString();
                txtSetno.Text = DataGridWarbBooking.Rows[Index].Cells[6].Value.ToString();
                txtSetno.Tag = DataGridWarbBooking.Rows[Index].Cells[0].Value.ToString();
                txtlength.Text = DataGridWarbBooking.Rows[Index].Cells[8].Value.ToString();
                txtEnds.Text = DataGridWarbBooking.Rows[Index].Cells[7].Value.ToString();
                txtWeight.Text = DataGridWarbBooking.Rows[Index].Cells[9].Value.ToString();
                dtpgrndt.Text = DataGridWarbBooking.Rows[Index].Cells[3].Value.ToString();
                cmbLocation.Text = DataGridWarbBooking.Rows[Index].Cells[12].Value.ToString();
                DataGridWarbBooking.Rows.Remove(DataGridWarbBooking.Rows[Index]);
                DataGridWarbBooking.ClearSelection();
                EditMode = true;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnHide_Click(object sender, EventArgs e)
        {
            grSearch.Visible = false;
        }

        private void DataGridCommon_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if(e.KeyCode == Keys.F7)
                {
                    grSearch.Visible = false;
                }
                else if(e.KeyCode == Keys.F6)
                {
                    SelectId = 1;
                    int Index = DataGridCommon.SelectedCells[0].RowIndex;
                    if (Fillid == 2)
                    {
                        txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    else if (Fillid == 3)
                    {
                        txtBeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                        txtBeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                    }
                    grSearch.Visible = false;
                    SelectId = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Date", Convert.ToDateTime(dtpgrndt.Text)), new SqlParameter("@Location", cmbLocation.Text), new SqlParameter("@Tag", "SelectbyLocation") };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetWarbBooking", para);
                LoadGetWarbBooking(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                SelectId = 1;
                int Index = DataGridCommon.SelectedCells[0].RowIndex;
                if (Fillid == 2)
                {
                    txtItem.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtItem.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                else if (Fillid == 3)
                {
                    txtBeam.Text = DataGridCommon.Rows[Index].Cells[1].Value.ToString();
                    txtBeam.Tag = DataGridCommon.Rows[Index].Cells[0].Value.ToString();
                }
                grSearch.Visible = false;
                SelectId = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Date", Convert.ToDateTime(dtpgrndt.Text)), new SqlParameter("@Location", cmbLocation.Text), new SqlParameter("@Tag", "SelectbyDtae") };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetWarbBooking", para);
                LoadGetWarbBooking(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
