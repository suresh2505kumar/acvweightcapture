﻿namespace ACVWeightCapture
{
    partial class FrmCutPice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grCutpice = new System.Windows.Forms.GroupBox();
            this.panelMain = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.lblWtPMtr = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbQc = new System.Windows.Forms.ComboBox();
            this.bntPrintBarcode = new System.Windows.Forms.Button();
            this.lblMendorName = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnReset = new System.Windows.Forms.Button();
            this.txtMender = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.DataGridCutPices = new System.Windows.Forms.DataGridView();
            this.lblSortNo = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNetWeight = new System.Windows.Forms.TextBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbBarcodeNoofPices = new System.Windows.Forms.ComboBox();
            this.cmbNoofPices = new System.Windows.Forms.ComboBox();
            this.touchScreen1 = new ACVWeightCapture.TouchScreen();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMeter = new System.Windows.Forms.TextBox();
            this.txtbarcode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grFront = new System.Windows.Forms.GroupBox();
            this.btnPrintBarCode = new System.Windows.Forms.Button();
            this.btnMending = new System.Windows.Forms.Button();
            this.btndelete = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.DataGridQC = new System.Windows.Forms.DataGridView();
            this.btnQc = new System.Windows.Forms.Button();
            this.btnRefersh = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.cmbQCSt = new System.Windows.Forms.ComboBox();
            this.dtpfilterDate = new System.Windows.Forms.DateTimePicker();
            this.grCutpice.SuspendLayout();
            this.panelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCutPices)).BeginInit();
            this.grFront.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridQC)).BeginInit();
            this.SuspendLayout();
            // 
            // grCutpice
            // 
            this.grCutpice.Controls.Add(this.panelMain);
            this.grCutpice.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grCutpice.Location = new System.Drawing.Point(6, 0);
            this.grCutpice.Name = "grCutpice";
            this.grCutpice.Size = new System.Drawing.Size(1185, 721);
            this.grCutpice.TabIndex = 0;
            this.grCutpice.TabStop = false;
            this.grCutpice.Enter += new System.EventHandler(this.grCutpice_Enter);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.label11);
            this.panelMain.Controls.Add(this.lblWtPMtr);
            this.panelMain.Controls.Add(this.label10);
            this.panelMain.Controls.Add(this.btnSave);
            this.panelMain.Controls.Add(this.cmbQc);
            this.panelMain.Controls.Add(this.bntPrintBarcode);
            this.panelMain.Controls.Add(this.lblMendorName);
            this.panelMain.Controls.Add(this.btnBack);
            this.panelMain.Controls.Add(this.label9);
            this.panelMain.Controls.Add(this.btnReset);
            this.panelMain.Controls.Add(this.txtMender);
            this.panelMain.Controls.Add(this.label8);
            this.panelMain.Controls.Add(this.label7);
            this.panelMain.Controls.Add(this.DataGridCutPices);
            this.panelMain.Controls.Add(this.lblSortNo);
            this.panelMain.Controls.Add(this.label6);
            this.panelMain.Controls.Add(this.txtNetWeight);
            this.panelMain.Controls.Add(this.btnOK);
            this.panelMain.Controls.Add(this.label5);
            this.panelMain.Controls.Add(this.cmbBarcodeNoofPices);
            this.panelMain.Controls.Add(this.cmbNoofPices);
            this.panelMain.Controls.Add(this.touchScreen1);
            this.panelMain.Controls.Add(this.dtpDate);
            this.panelMain.Controls.Add(this.txtWeight);
            this.panelMain.Controls.Add(this.label1);
            this.panelMain.Controls.Add(this.label4);
            this.panelMain.Controls.Add(this.label2);
            this.panelMain.Controls.Add(this.txtMeter);
            this.panelMain.Controls.Add(this.txtbarcode);
            this.panelMain.Controls.Add(this.label3);
            this.panelMain.Location = new System.Drawing.Point(6, 12);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(1173, 642);
            this.panelMain.TabIndex = 10;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Yellow;
            this.label11.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(99, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(864, 37);
            this.label11.TabIndex = 23;
            this.label11.Text = "Cut Piece";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // lblWtPMtr
            // 
            this.lblWtPMtr.AutoSize = true;
            this.lblWtPMtr.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWtPMtr.ForeColor = System.Drawing.Color.Red;
            this.lblWtPMtr.Location = new System.Drawing.Point(643, 126);
            this.lblWtPMtr.Name = "lblWtPMtr";
            this.lblWtPMtr.Size = new System.Drawing.Size(18, 26);
            this.lblWtPMtr.TabIndex = 22;
            this.lblWtPMtr.Text = "-";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(642, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(143, 26);
            this.label10.TabIndex = 21;
            this.label10.Text = "Weight Per Mtr";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Lime;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(987, 143);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(183, 85);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbQc
            // 
            this.cmbQc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQc.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbQc.FormattingEnabled = true;
            this.cmbQc.Items.AddRange(new object[] {
            "QC Pass",
            "Mending",
            "Rejection"});
            this.cmbQc.Location = new System.Drawing.Point(535, 185);
            this.cmbQc.Name = "cmbQc";
            this.cmbQc.Size = new System.Drawing.Size(126, 37);
            this.cmbQc.TabIndex = 6;
            this.cmbQc.SelectedIndexChanged += new System.EventHandler(this.cmbQc_SelectedIndexChanged);
            // 
            // bntPrintBarcode
            // 
            this.bntPrintBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bntPrintBarcode.BackColor = System.Drawing.Color.SpringGreen;
            this.bntPrintBarcode.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntPrintBarcode.Location = new System.Drawing.Point(987, 234);
            this.bntPrintBarcode.Name = "bntPrintBarcode";
            this.bntPrintBarcode.Size = new System.Drawing.Size(183, 85);
            this.bntPrintBarcode.TabIndex = 13;
            this.bntPrintBarcode.Text = "Print Barcode";
            this.bntPrintBarcode.UseVisualStyleBackColor = false;
            this.bntPrintBarcode.Click += new System.EventHandler(this.bntPrintBarcode_Click);
            // 
            // lblMendorName
            // 
            this.lblMendorName.AutoSize = true;
            this.lblMendorName.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMendorName.ForeColor = System.Drawing.Color.Red;
            this.lblMendorName.Location = new System.Drawing.Point(727, 55);
            this.lblMendorName.Name = "lblMendorName";
            this.lblMendorName.Size = new System.Drawing.Size(18, 26);
            this.lblMendorName.TabIndex = 20;
            this.lblMendorName.Text = "-";
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnBack.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(987, 416);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(183, 85);
            this.btnBack.TabIndex = 14;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(583, 55);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 26);
            this.label9.TabIndex = 19;
            this.label9.Text = "Mendor Name";
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.Yellow;
            this.btnReset.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(987, 325);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(183, 85);
            this.btnReset.TabIndex = 12;
            this.btnReset.Text = "Refresh";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtMender
            // 
            this.txtMender.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMender.Location = new System.Drawing.Point(398, 50);
            this.txtMender.Name = "txtMender";
            this.txtMender.Size = new System.Drawing.Size(166, 37);
            this.txtMender.TabIndex = 0;
            this.txtMender.Enter += new System.EventHandler(this.textBox1_Enter);
            this.txtMender.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            this.txtMender.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(266, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 26);
            this.label8.TabIndex = 18;
            this.label8.Text = "Mendor Code";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(422, 94);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 26);
            this.label7.TabIndex = 16;
            this.label7.Text = "Sort No";
            // 
            // DataGridCutPices
            // 
            this.DataGridCutPices.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridCutPices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridCutPices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCutPices.Location = new System.Drawing.Point(26, 226);
            this.DataGridCutPices.Name = "DataGridCutPices";
            this.DataGridCutPices.ReadOnly = true;
            this.DataGridCutPices.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataGridCutPices.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridCutPices.RowTemplate.Height = 30;
            this.DataGridCutPices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridCutPices.Size = new System.Drawing.Size(704, 360);
            this.DataGridCutPices.TabIndex = 15;
            // 
            // lblSortNo
            // 
            this.lblSortNo.AutoSize = true;
            this.lblSortNo.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSortNo.ForeColor = System.Drawing.Color.Red;
            this.lblSortNo.Location = new System.Drawing.Point(422, 126);
            this.lblSortNo.Name = "lblSortNo";
            this.lblSortNo.Size = new System.Drawing.Size(18, 26);
            this.lblSortNo.TabIndex = 14;
            this.lblSortNo.Text = "-";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(445, 595);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 26);
            this.label6.TabIndex = 13;
            this.label6.Text = "Net Weight";
            // 
            // txtNetWeight
            // 
            this.txtNetWeight.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNetWeight.Location = new System.Drawing.Point(560, 590);
            this.txtNetWeight.Name = "txtNetWeight";
            this.txtNetWeight.Size = new System.Drawing.Size(167, 37);
            this.txtNetWeight.TabIndex = 12;
            this.txtNetWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.Lime;
            this.btnOK.Location = new System.Drawing.Point(661, 184);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(67, 39);
            this.btnOK.TabIndex = 7;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(281, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 26);
            this.label5.TabIndex = 11;
            this.label5.Text = "No of Pieces";
            // 
            // cmbBarcodeNoofPices
            // 
            this.cmbBarcodeNoofPices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBarcodeNoofPices.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBarcodeNoofPices.FormattingEnabled = true;
            this.cmbBarcodeNoofPices.Location = new System.Drawing.Point(26, 185);
            this.cmbBarcodeNoofPices.Name = "cmbBarcodeNoofPices";
            this.cmbBarcodeNoofPices.Size = new System.Drawing.Size(248, 37);
            this.cmbBarcodeNoofPices.TabIndex = 3;
            this.cmbBarcodeNoofPices.SelectedIndexChanged += new System.EventHandler(this.cmbBarcodeNoofPices_SelectedIndexChanged);
            // 
            // cmbNoofPices
            // 
            this.cmbNoofPices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbNoofPices.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbNoofPices.FormattingEnabled = true;
            this.cmbNoofPices.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.cmbNoofPices.Location = new System.Drawing.Point(281, 122);
            this.cmbNoofPices.Name = "cmbNoofPices";
            this.cmbNoofPices.Size = new System.Drawing.Size(121, 37);
            this.cmbNoofPices.TabIndex = 2;
            this.cmbNoofPices.SelectedIndexChanged += new System.EventHandler(this.cmbNoofPices_SelectedIndexChanged);
            // 
            // touchScreen1
            // 
            this.touchScreen1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.touchScreen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.touchScreen1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.touchScreen1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.touchScreen1.Location = new System.Drawing.Point(735, 186);
            this.touchScreen1.Margin = new System.Windows.Forms.Padding(4);
            this.touchScreen1.Name = "touchScreen1";
            this.touchScreen1.Size = new System.Drawing.Size(228, 399);
            this.touchScreen1.TabIndex = 10;
            // 
            // dtpDate
            // 
            this.dtpDate.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(99, 52);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(156, 33);
            this.dtpDate.TabIndex = 2;
            // 
            // txtWeight
            // 
            this.txtWeight.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(408, 185);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(126, 37);
            this.txtWeight.TabIndex = 5;
            this.txtWeight.Enter += new System.EventHandler(this.txtWeight_Enter);
            this.txtWeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWeight_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(27, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 26);
            this.label1.TabIndex = 1;
            this.label1.Text = "Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(408, 158);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 26);
            this.label4.TabIndex = 7;
            this.label4.Text = "Weight";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(26, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "Barcode";
            // 
            // txtMeter
            // 
            this.txtMeter.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMeter.Location = new System.Drawing.Point(274, 185);
            this.txtMeter.Name = "txtMeter";
            this.txtMeter.Size = new System.Drawing.Size(133, 37);
            this.txtMeter.TabIndex = 4;
            this.txtMeter.Enter += new System.EventHandler(this.txtMeter_Enter);
            this.txtMeter.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtMeter_KeyDown);
            // 
            // txtbarcode
            // 
            this.txtbarcode.Font = new System.Drawing.Font("Calibri", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbarcode.Location = new System.Drawing.Point(26, 122);
            this.txtbarcode.Name = "txtbarcode";
            this.txtbarcode.Size = new System.Drawing.Size(249, 37);
            this.txtbarcode.TabIndex = 1;
            this.txtbarcode.Enter += new System.EventHandler(this.txtbarcode_Enter);
            this.txtbarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtbarcode_KeyDown);
            this.txtbarcode.Leave += new System.EventHandler(this.txtbarcode_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(281, 158);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 26);
            this.label3.TabIndex = 5;
            this.label3.Text = "Meter";
            // 
            // grFront
            // 
            this.grFront.Controls.Add(this.btnPrintBarCode);
            this.grFront.Controls.Add(this.btnMending);
            this.grFront.Controls.Add(this.btndelete);
            this.grFront.Controls.Add(this.txtSearch);
            this.grFront.Controls.Add(this.DataGridQC);
            this.grFront.Controls.Add(this.btnQc);
            this.grFront.Controls.Add(this.btnRefersh);
            this.grFront.Controls.Add(this.btnExit);
            this.grFront.Controls.Add(this.cmbQCSt);
            this.grFront.Controls.Add(this.dtpfilterDate);
            this.grFront.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grFront.Location = new System.Drawing.Point(6, 0);
            this.grFront.Name = "grFront";
            this.grFront.Size = new System.Drawing.Size(1185, 727);
            this.grFront.TabIndex = 23;
            this.grFront.TabStop = false;
            // 
            // btnPrintBarCode
            // 
            this.btnPrintBarCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintBarCode.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnPrintBarCode.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintBarCode.Location = new System.Drawing.Point(979, 279);
            this.btnPrintBarCode.Name = "btnPrintBarCode";
            this.btnPrintBarCode.Size = new System.Drawing.Size(200, 89);
            this.btnPrintBarCode.TabIndex = 32;
            this.btnPrintBarCode.Text = "Print Barcode";
            this.btnPrintBarCode.UseVisualStyleBackColor = false;
            this.btnPrintBarCode.Click += new System.EventHandler(this.btnPrintBarCode_Click);
            // 
            // btnMending
            // 
            this.btnMending.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMending.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnMending.Enabled = false;
            this.btnMending.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMending.Location = new System.Drawing.Point(979, 185);
            this.btnMending.Name = "btnMending";
            this.btnMending.Size = new System.Drawing.Size(200, 89);
            this.btnMending.TabIndex = 31;
            this.btnMending.Text = "Mending QC";
            this.btnMending.UseVisualStyleBackColor = false;
            this.btnMending.Click += new System.EventHandler(this.btnMending_Click);
            // 
            // btndelete
            // 
            this.btndelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btndelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btndelete.Enabled = false;
            this.btndelete.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndelete.Location = new System.Drawing.Point(979, 373);
            this.btndelete.Name = "btndelete";
            this.btndelete.Size = new System.Drawing.Size(200, 89);
            this.btndelete.TabIndex = 29;
            this.btndelete.Text = "Delete";
            this.btndelete.UseVisualStyleBackColor = false;
            this.btndelete.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(9, 15);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(1011, 31);
            this.txtSearch.TabIndex = 26;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // DataGridQC
            // 
            this.DataGridQC.AllowUserToAddRows = false;
            this.DataGridQC.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridQC.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.DataGridQC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridQC.EnableHeadersVisualStyles = false;
            this.DataGridQC.Location = new System.Drawing.Point(9, 47);
            this.DataGridQC.Name = "DataGridQC";
            this.DataGridQC.ReadOnly = true;
            this.DataGridQC.RowHeadersVisible = false;
            this.DataGridQC.RowTemplate.Height = 30;
            this.DataGridQC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridQC.Size = new System.Drawing.Size(943, 616);
            this.DataGridQC.TabIndex = 25;
            // 
            // btnQc
            // 
            this.btnQc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQc.BackColor = System.Drawing.Color.Lime;
            this.btnQc.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnQc.Location = new System.Drawing.Point(979, 91);
            this.btnQc.Name = "btnQc";
            this.btnQc.Size = new System.Drawing.Size(200, 89);
            this.btnQc.TabIndex = 24;
            this.btnQc.Text = "QC";
            this.btnQc.UseVisualStyleBackColor = false;
            this.btnQc.Click += new System.EventHandler(this.btnQc_Click);
            // 
            // btnRefersh
            // 
            this.btnRefersh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefersh.BackColor = System.Drawing.Color.Yellow;
            this.btnRefersh.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefersh.Location = new System.Drawing.Point(979, 467);
            this.btnRefersh.Name = "btnRefersh";
            this.btnRefersh.Size = new System.Drawing.Size(200, 89);
            this.btnRefersh.TabIndex = 25;
            this.btnRefersh.Text = "Refresh";
            this.btnRefersh.UseVisualStyleBackColor = false;
            this.btnRefersh.Click += new System.EventHandler(this.btnRefersh_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnExit.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(979, 561);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(200, 89);
            this.btnExit.TabIndex = 24;
            this.btnExit.Text = "Back";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // cmbQCSt
            // 
            this.cmbQCSt.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmbQCSt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQCSt.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbQCSt.FormattingEnabled = true;
            this.cmbQCSt.Items.AddRange(new object[] {
            "QC Pass",
            "Mending",
            "MQC Pass",
            "Rejection",
            "ALL"});
            this.cmbQCSt.Location = new System.Drawing.Point(988, 83);
            this.cmbQCSt.Name = "cmbQCSt";
            this.cmbQCSt.Size = new System.Drawing.Size(188, 31);
            this.cmbQCSt.TabIndex = 27;
            this.cmbQCSt.SelectedIndexChanged += new System.EventHandler(this.cmbQCSt_SelectedIndexChanged);
            // 
            // dtpfilterDate
            // 
            this.dtpfilterDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpfilterDate.CalendarFont = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpfilterDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpfilterDate.Location = new System.Drawing.Point(1044, 16);
            this.dtpfilterDate.Name = "dtpfilterDate";
            this.dtpfilterDate.Size = new System.Drawing.Size(132, 26);
            this.dtpfilterDate.TabIndex = 28;
            this.dtpfilterDate.ValueChanged += new System.EventHandler(this.dtpfilterDate_ValueChanged);
            // 
            // FrmCutPice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1203, 733);
            this.Controls.Add(this.grFront);
            this.Controls.Add(this.grCutpice);
            this.Name = "FrmCutPice";
            this.Text = "Cut Pices";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmCutPice_Load);
            this.grCutpice.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCutPices)).EndInit();
            this.grFront.ResumeLayout(false);
            this.grFront.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridQC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grCutpice;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.ComboBox cmbNoofPices;
        private TouchScreen touchScreen1;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMeter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbBarcodeNoofPices;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtbarcode;
        private System.Windows.Forms.Button bntPrintBarcode;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNetWeight;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblSortNo;
        private System.Windows.Forms.DataGridView DataGridCutPices;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMender;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblMendorName;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbQc;
        private System.Windows.Forms.Label lblWtPMtr;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox grFront;
        private System.Windows.Forms.Button btnQc;
        private System.Windows.Forms.DataGridView DataGridQC;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnRefersh;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ComboBox cmbQCSt;
        private System.Windows.Forms.DateTimePicker dtpfilterDate;
        private System.Windows.Forms.Button btndelete;
        private System.Windows.Forms.Button btnMending;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnPrintBarCode;
    }
}