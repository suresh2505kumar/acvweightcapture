﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ACVWeightCapture
{
    public partial class FrmPendingGRN : Form
    {
        public FrmPendingGRN()
        {
            InitializeComponent();
            this.FormClosing += FrmPendingGRN_FormClosing;
        }
        public static int uid;
        public static string GRNDate;
        public static string GRNNo;
        public static string Supplier;
        public static string DCNo;
        public static string DCDate;
        public static string VechileNo;
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bs = new BindingSource();
        private void FrmPendingGRN_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to go back ?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    this.Hide();
                    FrmIOSource io = new FrmIOSource();
                    io.Show();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do you really want to go back ?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            if (result == DialogResult.Yes)
            {
                this.Hide();
                FrmIOSource source = new FrmIOSource();
                source.Show();
            }
            else
            {
            }
         
        }

        private void btnCapture_Click(object sender, EventArgs e)
        {
            try
            {
                if(DataGridGRN.SelectedRows.Count != 0)
                {
                    int Index = DataGridGRN.SelectedCells[0].RowIndex;
                    uid = Convert.ToInt32(DataGridGRN.Rows[Index].Cells[0].Value.ToString());
                    GRNDate = DataGridGRN.Rows[Index].Cells[1].Value.ToString();
                    GRNNo = DataGridGRN.Rows[Index].Cells[2].Value.ToString();
                    Supplier = DataGridGRN.Rows[Index].Cells[3].Value.ToString();
                    DCNo = DataGridGRN.Rows[Index].Cells[4].Value.ToString();
                    DCDate = DataGridGRN.Rows[Index].Cells[1].Value.ToString();
                    VechileNo = DataGridGRN.Rows[Index].Cells[5].Value.ToString();
                    this.Hide();
                    FrmGRNWeightCapture weh = new FrmGRNWeightCapture();
                    weh.Show();
                }
                else
                {
                    MessageBox.Show("Select a DC Number to capture weight", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            
        }

        private void FrmPendingGRN_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridGRN.EnableHeadersVisualStyles = false;
                DataGridGRN.ColumnHeadersDefaultCellStyle.BackColor = Color.LightSeaGreen;
                grGRN.Height = this.Height - 50;
                grGRN.Width = this.Width - 250;
                DataGridGRN.Height = this.Height - 150;
                DataGridGRN.Width = this.Width - 270;
                txtSearch.Height = this.Height - 150;
                txtSearch.Width = this.Width - 270;                
                GetData();
                
            }
            catch (Exception)
            {
                MessageBox.Show("Server Not Connected", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            
        }
        public void LoadTitle()
        {
            DataGridGRN.DataSource = null;
            DataGridGRN.AutoGenerateColumns = false;
            DataGridGRN.ColumnCount = 5;
            DataGridGRN.Columns[0].Name = "Uid";
            DataGridGRN.Columns[0].HeaderText = "Uid";
            DataGridGRN.Columns[0].DataPropertyName = "Uid";
            DataGridGRN.Columns[0].Visible = false;
            DataGridGRN.Columns[1].Name = "GRN Date";
            DataGridGRN.Columns[1].HeaderText = "GRN Date";

            DataGridGRN.Columns[2].Name = "GRN No";
            DataGridGRN.Columns[2].HeaderText = "GRN No";

            DataGridGRN.Columns[3].Name = "Supplier";
            DataGridGRN.Columns[3].HeaderText = "Supplier";

            DataGridGRN.Columns[4].Name = "Vehile No";
            DataGridGRN.Columns[4].HeaderText = "Vehile No";            
          
        }
        protected void GetData()
        {
            SqlParameter[] para = { new SqlParameter("@Date", Convert.ToDateTime(dtpDate.Text)) };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetPendingGRN", para);
            bs.DataSource = dt;
            DataGridGRN.DataSource = null;
            DataGridGRN.AutoGenerateColumns = false;
            DataGridGRN.ColumnCount = 6;
            DataGridGRN.Columns[0].Name = "Uid";
            DataGridGRN.Columns[0].HeaderText = "Uid";
            DataGridGRN.Columns[0].DataPropertyName = "Uid";
            DataGridGRN.Columns[0].Visible = false;
            DataGridGRN.Columns[1].Name = "GRN Date";
            DataGridGRN.Columns[1].HeaderText = "GRN Date";
            DataGridGRN.Columns[1].DataPropertyName = "DocDate";
            DataGridGRN.Columns[1].DefaultCellStyle.Format = "dd-MMM-yyyy";
            DataGridGRN.Columns[1].Width = 200;
            DataGridGRN.Columns[2].Name = "GRN No";
            DataGridGRN.Columns[2].HeaderText = "GRN No";
            DataGridGRN.Columns[2].DataPropertyName = "DocNO";
            DataGridGRN.Columns[3].Name = "Supplier";
            DataGridGRN.Columns[3].HeaderText = "Supplier";          
            DataGridGRN.Columns[3].DataPropertyName = "Name";
            DataGridGRN.Columns[3].Width = 560;
            DataGridGRN.Columns[4].Name = "Dc NO";
            DataGridGRN.Columns[4].HeaderText = "Dc NO";
            DataGridGRN.Columns[4].DataPropertyName = "DcNO";
            DataGridGRN.Columns[5].Name = "Vehile No";
            DataGridGRN.Columns[5].HeaderText = "Vehile No";
            DataGridGRN.Columns[5].DataPropertyName = "Remarks";
            DataGridGRN.Columns[5].Width = 140;
            DataGridGRN.DataSource = bs;
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            bs.Filter = string.Format("DocNO like '%{0}%' or Name like '%{1}%' or DcNO Like '%{2}%'", txtSearch.Text, txtSearch.Text, txtSearch.Text);
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                GetData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                GetData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }
    }
}
