﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ACVWeightCapture
{
    public partial class FrmQc : Form
    {
        public FrmQc()
        {
            InitializeComponent();
            this.FormClosing += FrmQc_FormClosing;
        }

        private void FrmQc_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    DialogResult result = MessageBox.Show("Do you really exit the application?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                    if (result == DialogResult.Yes)
                    {
                        Process.GetCurrentProcess().Kill();
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmQc_Load(object sender, EventArgs e)
        {
            panelMain.Location = new Point(ClientSize.Width / 2 - panelMain.Size.Width / 2, ClientSize.Height / 2 - panelMain.Size.Height / 2);
            panelMain.Anchor = AnchorStyles.None;
        }

        private void BtnInSource_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                FrmFolding fold = new FrmFolding();
                fold.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnOutSource_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                FrmRolling rolling = new FrmRolling();
                rolling.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnCutpices_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                FrmCutPice CutPice = new FrmCutPice();
                CutPice.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void BtnBack_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult result = MessageBox.Show("Do you really exit the application?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    Process.GetCurrentProcess().Kill();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
