﻿namespace ACVWeightCapture
{
    partial class FrmQc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelMain = new System.Windows.Forms.Panel();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnCutpices = new System.Windows.Forms.Button();
            this.btnRolling = new System.Windows.Forms.Button();
            this.btnInSource = new System.Windows.Forms.Button();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.btnBack);
            this.panelMain.Controls.Add(this.btnCutpices);
            this.panelMain.Controls.Add(this.btnRolling);
            this.panelMain.Controls.Add(this.btnInSource);
            this.panelMain.Location = new System.Drawing.Point(172, 89);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(822, 361);
            this.panelMain.TabIndex = 5;
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnBack.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(516, 100);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(183, 137);
            this.btnBack.TabIndex = 15;
            this.btnBack.Text = "Exit";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // btnCutpices
            // 
            this.btnCutpices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnCutpices.BackColor = System.Drawing.Color.Yellow;
            this.btnCutpices.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCutpices.Location = new System.Drawing.Point(310, 100);
            this.btnCutpices.Name = "btnCutpices";
            this.btnCutpices.Size = new System.Drawing.Size(200, 137);
            this.btnCutpices.TabIndex = 4;
            this.btnCutpices.Text = "Cut Piece";
            this.btnCutpices.UseVisualStyleBackColor = false;
            this.btnCutpices.Click += new System.EventHandler(this.BtnCutpices_Click);
            // 
            // btnRolling
            // 
            this.btnRolling.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnRolling.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnRolling.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRolling.Location = new System.Drawing.Point(103, 100);
            this.btnRolling.Name = "btnRolling";
            this.btnRolling.Size = new System.Drawing.Size(200, 137);
            this.btnRolling.TabIndex = 3;
            this.btnRolling.Text = "Rolling";
            this.btnRolling.UseVisualStyleBackColor = false;
            this.btnRolling.Click += new System.EventHandler(this.BtnOutSource_Click);
            // 
            // btnInSource
            // 
            this.btnInSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnInSource.BackColor = System.Drawing.Color.Chartreuse;
            this.btnInSource.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInSource.Location = new System.Drawing.Point(101, 100);
            this.btnInSource.Name = "btnInSource";
            this.btnInSource.Size = new System.Drawing.Size(200, 137);
            this.btnInSource.TabIndex = 2;
            this.btnInSource.Text = "Folding";
            this.btnInSource.UseVisualStyleBackColor = false;
            this.btnInSource.Click += new System.EventHandler(this.BtnInSource_Click);
            // 
            // FrmQc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1068, 536);
            this.Controls.Add(this.panelMain);
            this.Name = "FrmQc";
            this.Text = "Quality Control";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmQc_Load);
            this.panelMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Button btnInSource;
        private System.Windows.Forms.Button btnRolling;
        private System.Windows.Forms.Button btnCutpices;
        private System.Windows.Forms.Button btnBack;
    }
}