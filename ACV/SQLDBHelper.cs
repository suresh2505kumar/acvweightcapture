﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ACVWeightCapture
{
   
    public class SQLDBHelper
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnStr"].ConnectionString);
        public DataTable  GetData(CommandType CmdType,string ComandText)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = ComandText;
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetData(CommandType CmdType, string ComandText,SqlParameter[] para)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = ComandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataSet GetMultipleData(CommandType CmdType, string ComandText, SqlParameter[] para)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = ComandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public int ExecuteQuery (CommandType CmdType,string CommandText,SqlParameter[] para)
        {
            int i = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                i = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return i;
        }
        public int ExecuteQuery(CommandType CmdType, string CommandText, SqlParameter[] para,SqlConnection conn)
        {
            int i = 0;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                i = cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return i;
        }
        public int ExecuteQuery(CommandType CmdType, string CommandText, SqlParameter[] para,int Index)
        {
            int i = 0;
            try
            {
                conn.Close();
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                i = Convert.ToInt32(para[Index].Value.ToString());//Solved
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return i;
        }
        public string ExecuteQueryString(CommandType CmdType, string CommandText, SqlParameter[] para, int Index)
        {
            string i = string.Empty;
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CmdType;
                cmd.CommandText = CommandText;
                cmd.Parameters.AddRange(para);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                i = para[Index].Value.ToString();//Solved
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return i;
        }
    }
}
