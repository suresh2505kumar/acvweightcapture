﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using CrystalDecisions.CrystalReports.Engine;
using System.Drawing.Printing;
using CrystalDecisions.Shared;
using System.Linq;
using System.Collections.Generic;

namespace ACVWeightCapture
{
    public partial class FrmSortDetails : Form
    {
        private TextBox focusedTextbox = null;
        public FrmSortDetails()
        {
            InitializeComponent();
            touchScreen1.OnUserControlButtonClicked += new TouchScreen.ButtonClickedEventHandler(touchScreen1_OnUserControlButtonClicked);
        }

        private void TouchScreen1_onUserKeyPress()
        {
            try
            {
                int r = Convert.ToInt32(txtRollNo.Text);
                SqlParameter[] para = { new SqlParameter("@RollNo", "Select"), new SqlParameter("@Tag", 0), new SqlParameter("@Uid", r) };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortDetail", para);
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Record Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    DataGridSortWeight.ClearSelection();
                    ID = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                    txtRollNo.Text = string.Empty;
                    txtRollNo.Text = dt.Rows[0]["RoolNo"].ToString();
                    txtRollNo.Tag = dt.Rows[0]["ID"].ToString();
                    string NetWt = dt.Rows[0]["GrossWt"].ToString();
                    txtWeight.Text = NetWt;
                    foreach (DataGridViewRow row in DataGridSortWeight.Rows)
                    {
                        string columnvalue = row.Cells[5].Value.ToString();//you can also use the column Index
                        if (txtRollNo.Text == columnvalue)
                        {
                            row.Selected = true;
                        }
                    }
                    txtWeight.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void touchScreen1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                if (focusedTextbox != null)
                {
                    if (b.Text == "Back")
                    {
                        if (focusedTextbox.Text.Length > 1)
                        {
                            focusedTextbox.Text = focusedTextbox.Text.Substring(0, focusedTextbox.Text.Length - 1);
                        }
                        else
                        {
                            focusedTextbox.Text = string.Empty;
                        }
                    }
                    else if (b.Text == "Clear")
                    {
                        focusedTextbox.Text = string.Empty;
                        focusedTextbox.Focus();
                    }
                    else if (b.Text == "Enter")
                    {
                        TouchScreen1_onUserKeyPress();
                    }
                    else if (b.Text == "TAB")
                    {
                        this.SelectNextControl(focusedTextbox, true, true, true, true);
                    }
                    else
                    {
                        if (MyGlobal.bTouch)
                            focusedTextbox.Text = b.Text;
                        else
                        {
                            MyGlobal.bTouch = false;
                            focusedTextbox.Text += b.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        SQLDBHelper db = new SQLDBHelper();
        int ID;

        private void FrmSortDetails_Load(object sender, EventArgs e)
        {
            DataGridSortWeight.EnableHeadersVisualStyles = false;
            DataGridSortWeight.ColumnHeadersDefaultCellStyle.BackColor = Color.LightSeaGreen;
            grWeight.Height = this.Height - 100;
            DataGridSortWeight.Height = this.Height - 100;
            grWeight.Width = this.Width - 250;
            DataGridSortWeight.Width = this.Width - 250;
            cmbCaptured.SelectedIndex = 0;
            getData();
            chckbatch.Checked = true;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want close the Application ?", "Close", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res == DialogResult.Yes)
                {
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        public void LoadTitle(DataTable dt, string str)
        {
            try
            {
                DataGridSortWeight.DataSource = null;
                DataGridSortWeight.AutoGenerateColumns = false;
                DataGridSortWeight.ColumnCount = 17;
                DataGridSortWeight.Columns[0].Name = "Uid";
                DataGridSortWeight.Columns[0].HeaderText = "Uid";
                DataGridSortWeight.Columns[0].DataPropertyName = "Uid";
                DataGridSortWeight.Columns[0].Visible = false;
                DataGridSortWeight.Columns[1].Name = "SlNo";
                DataGridSortWeight.Columns[1].HeaderText = "SlNo";
                DataGridSortWeight.Columns[1].Width = 50;

                DataGridSortWeight.Columns[2].Name = "Time";
                if (str == "Loaded")
                {
                    DataGridSortWeight.Columns[2].HeaderText = "Time";
                }
                else
                {
                    DataGridSortWeight.Columns[2].HeaderText = "Time";
                }
                DataGridSortWeight.Columns[2].Width = 60;
                DataGridSortWeight.Columns[3].Name = "LoomNo";
                DataGridSortWeight.Columns[3].HeaderText = "LoomNo";
                DataGridSortWeight.Columns[3].Width = 70;

                DataGridSortWeight.Columns[4].Name = "SortNo";
                DataGridSortWeight.Columns[4].HeaderText = "SortNo";
                DataGridSortWeight.Columns[4].Width = 150;

                DataGridSortWeight.Columns[5].Name = "RoolNo";
                DataGridSortWeight.Columns[5].HeaderText = "RollNo";
                DataGridSortWeight.Columns[5].Width = 100;

                DataGridSortWeight.Columns[6].Name = "SpindleNo";
                DataGridSortWeight.Columns[6].HeaderText = "Spindle";
                DataGridSortWeight.Columns[6].Width = 60;

                DataGridSortWeight.Columns[7].Name = "TareWt";
                DataGridSortWeight.Columns[7].HeaderText = "TareWt";
                DataGridSortWeight.Columns[7].Width = 65;
                DataGridSortWeight.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridSortWeight.Columns[8].Name = "GrossWt";
                DataGridSortWeight.Columns[8].HeaderText = "GrossWt";
                DataGridSortWeight.Columns[8].Width = 90;
                DataGridSortWeight.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridSortWeight.Columns[9].Name = "Netwt";
                DataGridSortWeight.Columns[9].HeaderText = "Netwt";
                DataGridSortWeight.Columns[9].Width = 90;
                DataGridSortWeight.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridSortWeight.Columns[10].Name = "Wt Length";
                DataGridSortWeight.Columns[10].HeaderText = "Wt Length";
                DataGridSortWeight.Columns[10].Width = 80;
                DataGridSortWeight.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridSortWeight.Columns[11].Name = "Barcode";
                DataGridSortWeight.Columns[11].HeaderText = "Barcode";
                DataGridSortWeight.Columns[11].Visible = false;

                DataGridSortWeight.Columns[12].Name = "PickLength";
                DataGridSortWeight.Columns[12].HeaderText = "Pk Length";
                DataGridSortWeight.Columns[12].Width = 80;

                DataGridSortWeight.Columns[13].Name = "Std W/Mtr";
                DataGridSortWeight.Columns[13].HeaderText = "Std W/Mtr";
                DataGridSortWeight.Columns[13].Width = 60;

                DataGridSortWeight.Columns[14].Name = "Pk W/Mtr";
                DataGridSortWeight.Columns[14].HeaderText = "Pk W/Mtr";
                DataGridSortWeight.Columns[14].Width = 80;

                DataGridSortWeight.Columns[15].Name = "Difference";
                DataGridSortWeight.Columns[15].HeaderText = "Difference";
                DataGridSortWeight.Columns[15].Width = 80;
                DataGridSortWeight.Columns[16].Name = "Shift";
                DataGridSortWeight.Columns[16].HeaderText = "Shift";
                DataGridSortWeight.Columns[16].Visible = false;

                if (dt.Rows.Count != 0)
                {
                    DataGridSortWeight.Columns[0].DataPropertyName = "Id";
                    DataGridSortWeight.Columns[2].DataPropertyName = "CompletedOn";
                    DataGridSortWeight.Columns[3].DataPropertyName = "LoomNo";
                    DataGridSortWeight.Columns[4].DataPropertyName = "SortNo";
                    DataGridSortWeight.Columns[5].DataPropertyName = "RoolNo";
                    DataGridSortWeight.Columns[6].DataPropertyName = "SpindleNo";
                    DataGridSortWeight.Columns[7].DataPropertyName = "TareWt";
                    DataGridSortWeight.Columns[8].DataPropertyName = "GrossWt";
                    DataGridSortWeight.Columns[9].DataPropertyName = "Netwt";
                    DataGridSortWeight.Columns[10].DataPropertyName = "Lengn";
                    DataGridSortWeight.Columns[11].DataPropertyName = "Barcode";
                    DataGridSortWeight.Columns[12].DataPropertyName = "PickLength";
                    DataGridSortWeight.Columns[13].DataPropertyName = "WTMtr";
                    DataGridSortWeight.Columns[14].DataPropertyName = "PwPm";

                    DataGridSortWeight.Columns[16].DataPropertyName = "ShiftName";
                    DataGridSortWeight.DataSource = dt;
                }
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataGridSortWeight.Rows[i].Cells[1].Value = i + 1;
                }
                CellvalueChnaged();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void CellvalueChnaged()
        {
            
            for (int i = 0; i < DataGridSortWeight.Rows.Count; i++)
            {
                decimal stdWtMtr = 0;
                decimal PickWtMtr = 0;
                stdWtMtr = Convert.ToDecimal(DataGridSortWeight.Rows[i].Cells[13].Value);
                if(DataGridSortWeight.Rows[i].Cells[14].Value == null || DataGridSortWeight.Rows[i].Cells[14].Value.ToString() == "")
                {
                    PickWtMtr = 0;
                }
                else
                {
                    PickWtMtr = Convert.ToDecimal(DataGridSortWeight.Rows[i].Cells[14].Value);
                }
                DataGridSortWeight.Rows[i].Cells[15].Value = (PickWtMtr- stdWtMtr).ToString("0");
            }
        }

        protected void getData()
        {
            try
            {
                SqlParameter[] para = { new SqlParameter("@Type", cmbCaptured.Text), new SqlParameter("@Date", Convert.ToDateTime(dtpDate.Text).ToString("yyyy-MM-dd")) };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortDetailNew_V1", para);
                LoadTitle(dt, cmbCaptured.Text);
                CellValueChanged();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSortWeight_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                txtRollNo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FrmSortDetails_Enter(object sender, EventArgs e)
        {

        }

        private void FrmSortDetails_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up) || e.KeyCode.Equals(Keys.Down))
            {
                MoveUpDown(e.KeyCode == Keys.Up);
            }
            e.Handled = true;
        }
        private void MoveUpDown(bool goUp)
        {
            try
            {
                int currentRowindex = DataGridSortWeight.SelectedCells[0].OwningRow.Index;
                int newRowIndex = currentRowindex + (goUp ? -1 : 1);
                if (newRowIndex > -1 && newRowIndex < DataGridSortWeight.Rows.Count)
                {
                    DataGridSortWeight.ClearSelection();
                    DataGridSortWeight.Rows[newRowIndex].Selected = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtRollNo.Tag.ToString() == string.Empty)
                {
                    int r = Convert.ToInt32(txtRollNo.Text);
                    SqlParameter[] para = { new SqlParameter("@RollNo", "Select"), new SqlParameter("@Tag", 0), new SqlParameter("@Uid", r) };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortDetail", para);
                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show("Record Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        DataGridSortWeight.ClearSelection();
                        ID = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                        txtRollNo.Text = string.Empty;
                        txtRollNo.Text = dt.Rows[0]["RoolNo"].ToString();
                        txtRollNo.Tag = dt.Rows[0]["ID"].ToString();
                        string NetWt = dt.Rows[0]["GrossWt"].ToString();
                        txtWeight.Text = NetWt;
                        foreach (DataGridViewRow row in DataGridSortWeight.Rows)
                        {
                            string columnvalue = row.Cells[5].Value.ToString();//you can also use the column Index
                            if (txtRollNo.Text == columnvalue)
                            {
                                row.Selected = true;
                            }
                        }
                    }
                }

                string Barcode = string.Empty;
                Barcode = txtRollNo.Text;

                if (DataGridSortWeight.SelectedRows.Count != 0)
                {
                    int index = DataGridSortWeight.SelectedCells[0].RowIndex;
                    //ID = Convert.ToInt32(DataGridSortWeight.Rows[index].Cells[0].Value.ToString());
                    ID = Convert.ToInt32(txtRollNo.Tag);
                    if (ID == 0)
                    {
                        MessageBox.Show("Roll Number should be greater than Zero!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    decimal WtMtr; decimal Length;
                    if (DataGridSortWeight.Rows[index].Cells[13].Value.ToString() == "")
                    {
                        WtMtr = 0;
                    }
                    else
                    {
                        WtMtr = Convert.ToDecimal(DataGridSortWeight.Rows[index].Cells[13].Value.ToString());
                    }
                    decimal GrossWt = Convert.ToDecimal(txtWeight.Text);
                    decimal TareWt = Convert.ToDecimal(DataGridSortWeight.Rows[index].Cells[7].Value.ToString());
                    decimal NetWt = GrossWt - TareWt;
                    if (WtMtr == 0)
                    {
                        Length = 0;
                    }
                    else
                    {
                        Length = NetWt * 1000 / WtMtr;
                    }

                    SqlParameter[] para1 = {
                        new SqlParameter("@ID",ID),
                        new SqlParameter("@TareWt",TareWt),
                        new SqlParameter("@GrossWt",GrossWt),
                        new SqlParameter("@NetWt",NetWt),
                        new SqlParameter("@Barcode",Barcode),
                        new SqlParameter("@Length",Length.ToString("0.000"))
                    };
                    int i = db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateSortDet", para1);
                    DataGridSortWeight.Rows[index].Cells[8].Value = GrossWt;
                    DataGridSortWeight.Rows[index].Cells[9].Value = NetWt;
                    txtWeight.Text = string.Empty;
                    txtRollNo.Text = string.Empty;
                    PrintBarcode(ID);
                    getData();
                    txtRollNo.Focus();
                }
                else
                {
                    MessageBox.Show("Select Beam  Number ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        private void PrintBarcode(int id)
        {
            try
            {
                if (chckbatch.Checked == true)
                {
                    FileStream fs;
                    SqlParameter[] para = { new SqlParameter("@Id", id) };
                    DataSet ds = db.GetMultipleData(CommandType.StoredProcedure, "SP_PrintBarcodeSortNew", para);
                    DataTable dtBarcode = ds.Tables[1];
                    DataTable dt = ds.Tables[0];
                    fs = new FileStream(Application.StartupPath + "\\Sortbarcode.txt", FileMode.Truncate, FileAccess.Write);
                    fs.Close();
                    for (int i = 0; i < dtBarcode.Rows.Count; i++)
                    {
                        string Var = dtBarcode.Rows[i]["VarType"].ToString();
                        if (Var == "Static")
                        {
                            File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", dtBarcode.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                        }
                        else
                        {
                            int LineNUmber = Convert.ToInt32(dtBarcode.Rows[i]["LineNumber"].ToString());
                            int Index = DataGridSortWeight.SelectedCells[0].RowIndex;
                            if (LineNUmber == 36)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[0]["LoomNo"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 37)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[0]["SortNo"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 38)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[0]["JobCardNo"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 39)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[0]["RollNo"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 40)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[0]["WTMtr"].ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 41)
                            {
                                string dte = dt.Rows[0]["CompletedOn"].ToString();
                                if (dte == "")
                                {
                                    dte = "";
                                }
                                else
                                {
                                    DateTime d = Convert.ToDateTime(dte);
                                    dte = d.ToString("dd.MM.yyyy");
                                }
                                string Shift = dt.Rows[0]["CompletedShiftType"].ToString();
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dte + "/" + Shift + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 42)
                            {
                                decimal GrossWt = Convert.ToDecimal(dt.Rows[0]["Grosswt"].ToString());
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + GrossWt.ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 43)
                            {
                                decimal Tare = Convert.ToDecimal(dt.Rows[0]["TareWt"].ToString());
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Tare.ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 44)
                            {
                                decimal GrossWt = Convert.ToDecimal(dt.Rows[0]["Grosswt"].ToString());
                                decimal Tare = Convert.ToDecimal(dt.Rows[0]["TareWt"].ToString());
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + (GrossWt - Tare).ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 45)
                            {
                                string Len = dt.Rows[0]["Lengn"].ToString();
                                decimal Length;
                                if (Len == "")
                                {
                                    Length = 0;
                                }
                                else
                                {
                                    Length = Convert.ToDecimal(Len);
                                }
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Length.ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 47)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[0]["Barcode"].ToString() + "\"";
                                text = text.Replace("\r\n", string.Empty);
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 48)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[0]["Barcode"].ToString() + "\"";
                                text = text.Replace("\r\n", string.Empty);
                                File.AppendAllText(Application.StartupPath + "\\Sortbarcode.txt", text + Environment.NewLine);
                            }
                        }
                    }
                    int PrintCount = 0;
                    string cnt = dt.Rows[0]["RollCnt"].ToString();
                    if (cnt == "")
                    {
                        PrintCount = 1;
                    }
                    else
                    {
                        PrintCount = Convert.ToInt32(cnt);
                    }
                    for (int i = 0; i < PrintCount; i++)
                    {
                        Process.Start(Application.StartupPath + "\\BPSort.bat");
                    }
                }
                else
                {
                    SqlParameter[] para = { new SqlParameter("@Id", id) };
                    DataTable dtBarcode = new DataTable();
                    dtBarcode = db.GetData(CommandType.StoredProcedure, "SP_PrintBarcodeSort", para);
                    //DataTable dtBarcodeLine = ds.Tables[1];
                    //Generate Barcode
                    KeepDynamic.Barcode.CrystalReport.BarCode br = new KeepDynamic.Barcode.CrystalReport.BarCode();
                    br.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
                    br.CodeText = dtBarcode.Rows[0]["Barcode"].ToString();
                    br.DisplayCodeText = false;
                    byte[] bt = br.drawBarcodeAsBytes();
                    //Date Shift merge
                    string dte = dtBarcode.Rows[0]["CompletedOn"].ToString();
                    if (dte == "")
                    {
                        dte = "";
                    }
                    else
                    {
                        DateTime d = Convert.ToDateTime(dte);
                        dte = d.ToString("dd.MM.yyyy");
                    }
                    string Shift = dtBarcode.Rows[0]["CompletedShiftType"].ToString();
                    // Net weight Calculation
                    decimal GrossWt = Convert.ToDecimal(dtBarcode.Rows[0]["Grosswt"].ToString());
                    decimal TareWt = Convert.ToDecimal(dtBarcode.Rows[0]["TareWt"].ToString());
                    decimal NetWt = GrossWt - TareWt;

                    //Length Calculation
                    string Len = dtBarcode.Rows[0]["Lengn"].ToString();
                    decimal Length;
                    if (Len == "")
                    {
                        Length = 0;
                    }
                    else
                    {
                        Length = Convert.ToDecimal(Len);
                    }

                    DataTable dt = new DataTable();
                    dt.Columns.Add("Barcode", typeof(byte[]));
                    dt.Columns.Add("LoomNo", typeof(string));
                    dt.Columns.Add("SortNo", typeof(string));
                    dt.Columns.Add("JobNo", typeof(string));
                    dt.Columns.Add("RollNo", typeof(string));
                    dt.Columns.Add("WtPerMtr", typeof(string));
                    dt.Columns.Add("DateShift", typeof(string));
                    dt.Columns.Add("GWt", typeof(decimal));
                    dt.Columns.Add("TWt", typeof(decimal));
                    dt.Columns.Add("NWt", typeof(decimal));
                    dt.Columns.Add("Length", typeof(string));
                    dt.Columns.Add("BarText", typeof(string));

                    DataRow row = dt.NewRow();
                    row["Barcode"] = bt;
                    row["LoomNo"] = dtBarcode.Rows[0]["LoomNo"].ToString();
                    row["SortNo"] = dtBarcode.Rows[0]["SortNo"].ToString();
                    row["JobNo"] = dtBarcode.Rows[0]["JobCardNo"].ToString();
                    row["RollNo"] = dtBarcode.Rows[0]["RollNo"].ToString();
                    row["WtPerMtr"] = dtBarcode.Rows[0]["WTMtr"].ToString();
                    row["DateShift"] = dte + " / " + Shift;
                    row["GWt"] = Convert.ToDecimal(dtBarcode.Rows[0]["Grosswt"].ToString()).ToString("0.000");
                    row["TWt"] = Convert.ToDecimal(dtBarcode.Rows[0]["TareWt"].ToString()).ToString("0.000");
                    row["NWt"] = NetWt.ToString("0.000");
                    row["Length"] = Length.ToString("0.000");
                    row["BarText"] = dtBarcode.Rows[0]["Barcode"].ToString();
                    dt.Rows.Add(row);
                    ReportDocument doc = new ReportDocument();
                    doc.Load(Application.StartupPath + "\\CrySortBarcode.rpt");
                    doc.SetDataSource(dt);
                    PrintDocument pDoc = new PrintDocument();
                    PrintLayoutSettings PrintLayout = new PrintLayoutSettings();
                    PrinterSettings printerSettings = new PrinterSettings();
                    printerSettings.PrinterName = pDoc.PrinterSettings.PrinterName;
                    PageSettings pSettings = new PageSettings(printerSettings);
                    pSettings.PrinterSettings.Copies = 1;
                    doc.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
                    doc.PrintOptions.PrinterDuplex = PrinterDuplex.Simplex;
                    doc.PrintToPrinter(printerSettings, pSettings, false, PrintLayout);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void bntPrintBarcode_Click(object sender, EventArgs e)
        {
            try
            {
                int Index = DataGridSortWeight.SelectedCells[0].RowIndex;
                ID = Convert.ToInt32(DataGridSortWeight.Rows[Index].Cells[0].Value.ToString());
                PrintBarcode(ID);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtRollNo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    int r = Convert.ToInt32(txtRollNo.Text);
                    SqlParameter[] para = { new SqlParameter("@RollNo", "Select"), new SqlParameter("@Tag", 0), new SqlParameter("@Uid", r) };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortDetail", para);
                    if (dt.Rows.Count == 0)
                    {
                        MessageBox.Show("Record Not Found", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        DataGridSortWeight.ClearSelection();
                        ID = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                        txtRollNo.Text = string.Empty;
                        txtRollNo.Text = dt.Rows[0]["RoolNo"].ToString();
                        txtRollNo.Tag = dt.Rows[0]["ID"].ToString();
                        string NetWt = dt.Rows[0]["GrossWt"].ToString();
                        txtWeight.Text = NetWt;
                        foreach (DataGridViewRow row in DataGridSortWeight.Rows)
                        {
                            string columnvalue = row.Cells[5].Value.ToString();//you can also use the column Index
                            if (txtRollNo.Text == columnvalue)
                            {
                                row.Selected = true;
                            }
                        }
                        txtWeight.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void chckNull_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                getData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtWeight_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSave_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbCaptured_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                getData();
                txtRollNo.Text = string.Empty;
                txtRollNo.Tag = string.Empty;
                txtWeight.Text = string.Empty;
                txtRollNo.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridSortWeight_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
        }
        protected void CellValueChanged()
        {
            try
            {
                foreach (DataGridViewRow row in DataGridSortWeight.Rows)
                {
                    if (row.Cells[9].Value.ToString() != "")
                    {
                        decimal Diff = Convert.ToDecimal(row.Cells[15].Value);
                        if(Diff > 0)
                        {
                            row.DefaultCellStyle.BackColor = Color.LightGreen;
                        }
                        else
                        {
                            row.DefaultCellStyle.BackColor = Color.MediumSpringGreen;
                        }
                    }
                    else
                    {
                        row.DefaultCellStyle.BackColor = Color.Yellow;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBarReport_Click(object sender, EventArgs e)
        {
            KeepDynamic.Barcode.CrystalReport.BarCode br = new KeepDynamic.Barcode.CrystalReport.BarCode();
            br.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
            br.CodeText = txtRollNo.Text;
            br.DisplayCodeText = false;
            byte[] bt = br.drawBarcodeAsBytes();
            DataTable dt = new DataTable();
            dt.Columns.Add("Barcode", typeof(byte[]));
            DataRow dr = dt.NewRow();
            dr["Barcode"] = bt;
            dt.Rows.Add(dr);
            ReportDocument doc = new ReportDocument();
            doc.Load(@"D:\ACV\ACVWeightCapture\ACV\CrySortBarcode.rpt");
            TextObject Barcode = (TextObject)doc.ReportDefinition.ReportObjects["Text11"];
            Barcode.Text = txtRollNo.Text;
            doc.SetDataSource(dt);
        }

        private void dtpDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                getData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void chckNumpad_CheckedChanged(object sender, EventArgs e)
        {
            if (chckNumpad.Checked == true)
            {
                touchScreen1.Visible = true;
            }
            else
            {
                touchScreen1.Visible = false;
            }
        }

        private void txtRollNo_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void txtWeight_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            getData();
            txtRollNo.Text = string.Empty;
            txtRollNo.Tag = string.Empty;
            txtWeight.Text = string.Empty;
            cmbCaptured.SelectedIndex = 0;
            txtRollNo.Focus();
        }
    }
}
