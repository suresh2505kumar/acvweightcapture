﻿namespace ACVWeightCapture
{
    partial class FrmGRNWeightCapture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grWeight = new System.Windows.Forms.GroupBox();
            this.touchScreen1 = new ACVWeightCapture.TouchScreen();
            this.txtVechileNo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDCNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.DataGridGRNWeight = new System.Windows.Forms.DataGridView();
            this.txtDcDate = new System.Windows.Forms.TextBox();
            this.txtSupplier = new System.Windows.Forms.TextBox();
            this.txtGRNDate = new System.Windows.Forms.TextBox();
            this.txtGRNNo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.bntPrintBarcode = new System.Windows.Forms.Button();
            this.txtBeamNo = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.txtTotalActual = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTotalNet = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txttotalTar = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtTotalDiff = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.chckNumpad = new System.Windows.Forms.CheckBox();
            this.chckbatch = new System.Windows.Forms.CheckBox();
            this.grWeight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGRNWeight)).BeginInit();
            this.SuspendLayout();
            // 
            // grWeight
            // 
            this.grWeight.AutoSize = true;
            this.grWeight.Controls.Add(this.touchScreen1);
            this.grWeight.Controls.Add(this.txtVechileNo);
            this.grWeight.Controls.Add(this.label8);
            this.grWeight.Controls.Add(this.txtDCNo);
            this.grWeight.Controls.Add(this.label7);
            this.grWeight.Controls.Add(this.DataGridGRNWeight);
            this.grWeight.Controls.Add(this.txtDcDate);
            this.grWeight.Controls.Add(this.txtSupplier);
            this.grWeight.Controls.Add(this.txtGRNDate);
            this.grWeight.Controls.Add(this.txtGRNNo);
            this.grWeight.Controls.Add(this.label3);
            this.grWeight.Controls.Add(this.label4);
            this.grWeight.Controls.Add(this.label2);
            this.grWeight.Controls.Add(this.label1);
            this.grWeight.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grWeight.Location = new System.Drawing.Point(8, 1);
            this.grWeight.Name = "grWeight";
            this.grWeight.Size = new System.Drawing.Size(1112, 533);
            this.grWeight.TabIndex = 0;
            this.grWeight.TabStop = false;
            // 
            // touchScreen1
            // 
            this.touchScreen1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.touchScreen1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.touchScreen1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.touchScreen1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.touchScreen1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.touchScreen1.Location = new System.Drawing.Point(888, 16);
            this.touchScreen1.Margin = new System.Windows.Forms.Padding(4);
            this.touchScreen1.Name = "touchScreen1";
            this.touchScreen1.Size = new System.Drawing.Size(224, 391);
            this.touchScreen1.TabIndex = 13;
            this.touchScreen1.Visible = false;
            // 
            // txtVechileNo
            // 
            this.txtVechileNo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVechileNo.Location = new System.Drawing.Point(954, 38);
            this.txtVechileNo.Name = "txtVechileNo";
            this.txtVechileNo.Size = new System.Drawing.Size(152, 31);
            this.txtVechileNo.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(950, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 23);
            this.label8.TabIndex = 11;
            this.label8.Text = "Vehile No";
            // 
            // txtDCNo
            // 
            this.txtDCNo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDCNo.Location = new System.Drawing.Point(612, 38);
            this.txtDCNo.Name = "txtDCNo";
            this.txtDCNo.Size = new System.Drawing.Size(152, 31);
            this.txtDCNo.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(612, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 23);
            this.label7.TabIndex = 9;
            this.label7.Text = "DC No";
            // 
            // DataGridGRNWeight
            // 
            this.DataGridGRNWeight.AllowUserToAddRows = false;
            this.DataGridGRNWeight.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DataGridGRNWeight.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DataGridGRNWeight.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridGRNWeight.Location = new System.Drawing.Point(14, 81);
            this.DataGridGRNWeight.Name = "DataGridGRNWeight";
            this.DataGridGRNWeight.ReadOnly = true;
            this.DataGridGRNWeight.RowHeadersVisible = false;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DataGridGRNWeight.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.DataGridGRNWeight.RowTemplate.Height = 30;
            this.DataGridGRNWeight.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DataGridGRNWeight.Size = new System.Drawing.Size(1081, 425);
            this.DataGridGRNWeight.TabIndex = 8;
            this.DataGridGRNWeight.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridGRNWeight_CellValueChanged);
            this.DataGridGRNWeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DataGridGRNWeight_KeyDown);
            this.DataGridGRNWeight.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DataGridGRNWeight_MouseClick);
            // 
            // txtDcDate
            // 
            this.txtDcDate.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDcDate.Location = new System.Drawing.Point(782, 38);
            this.txtDcDate.Name = "txtDcDate";
            this.txtDcDate.Size = new System.Drawing.Size(152, 31);
            this.txtDcDate.TabIndex = 7;
            // 
            // txtSupplier
            // 
            this.txtSupplier.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSupplier.Location = new System.Drawing.Point(331, 38);
            this.txtSupplier.Name = "txtSupplier";
            this.txtSupplier.Size = new System.Drawing.Size(269, 31);
            this.txtSupplier.TabIndex = 6;
            // 
            // txtGRNDate
            // 
            this.txtGRNDate.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGRNDate.Location = new System.Drawing.Point(174, 38);
            this.txtGRNDate.Name = "txtGRNDate";
            this.txtGRNDate.Size = new System.Drawing.Size(146, 31);
            this.txtGRNDate.TabIndex = 5;
            // 
            // txtGRNNo
            // 
            this.txtGRNNo.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGRNNo.Location = new System.Drawing.Point(14, 38);
            this.txtGRNNo.Name = "txtGRNNo";
            this.txtGRNNo.Size = new System.Drawing.Size(146, 31);
            this.txtGRNNo.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(778, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "DC Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(331, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 23);
            this.label4.TabIndex = 2;
            this.label4.Text = "Supplier";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(174, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "GRN Date";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "GRN No";
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btnBack.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Location = new System.Drawing.Point(1058, 561);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(188, 85);
            this.btnBack.TabIndex = 5;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1120, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 33);
            this.label5.TabIndex = 10;
            this.label5.Text = "Beam No";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1120, 185);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 33);
            this.label6.TabIndex = 11;
            this.label6.Text = "Weight";
            // 
            // txtWeight
            // 
            this.txtWeight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtWeight.Font = new System.Drawing.Font("Calibri", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWeight.Location = new System.Drawing.Point(1058, 221);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(188, 66);
            this.txtWeight.TabIndex = 0;
            this.txtWeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtWeight.Enter += new System.EventHandler(this.txtWeight_Enter);
            this.txtWeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtWeight_KeyDown);
            // 
            // bntPrintBarcode
            // 
            this.bntPrintBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bntPrintBarcode.BackColor = System.Drawing.Color.SpringGreen;
            this.bntPrintBarcode.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntPrintBarcode.Location = new System.Drawing.Point(1058, 472);
            this.bntPrintBarcode.Name = "bntPrintBarcode";
            this.bntPrintBarcode.Size = new System.Drawing.Size(188, 85);
            this.bntPrintBarcode.TabIndex = 4;
            this.bntPrintBarcode.Text = "Print Barcode";
            this.bntPrintBarcode.UseVisualStyleBackColor = false;
            this.bntPrintBarcode.Click += new System.EventHandler(this.bntPrintBarcode_Click);
            // 
            // txtBeamNo
            // 
            this.txtBeamNo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBeamNo.Font = new System.Drawing.Font("Calibri", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBeamNo.Location = new System.Drawing.Point(1063, 115);
            this.txtBeamNo.Multiline = true;
            this.txtBeamNo.Name = "txtBeamNo";
            this.txtBeamNo.Size = new System.Drawing.Size(185, 67);
            this.txtBeamNo.TabIndex = 1;
            this.txtBeamNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.Lime;
            this.btnSave.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Location = new System.Drawing.Point(1058, 296);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(188, 85);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.BackColor = System.Drawing.Color.Yellow;
            this.btnReset.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(1058, 384);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(188, 85);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Refresh";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // txtTotalActual
            // 
            this.txtTotalActual.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalActual.Location = new System.Drawing.Point(960, 593);
            this.txtTotalActual.Name = "txtTotalActual";
            this.txtTotalActual.Size = new System.Drawing.Size(94, 26);
            this.txtTotalActual.TabIndex = 14;
            this.txtTotalActual.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(960, 620);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 18);
            this.label9.TabIndex = 13;
            this.label9.Text = "Actual Wt";
            // 
            // txtTotalNet
            // 
            this.txtTotalNet.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalNet.Location = new System.Drawing.Point(858, 593);
            this.txtTotalNet.Name = "txtTotalNet";
            this.txtTotalNet.Size = new System.Drawing.Size(94, 26);
            this.txtTotalNet.TabIndex = 18;
            this.txtTotalNet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(858, 620);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 18);
            this.label10.TabIndex = 17;
            this.label10.Text = "Net Wt";
            // 
            // txttotalTar
            // 
            this.txttotalTar.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttotalTar.Location = new System.Drawing.Point(757, 593);
            this.txttotalTar.Name = "txttotalTar";
            this.txttotalTar.Size = new System.Drawing.Size(94, 26);
            this.txttotalTar.TabIndex = 20;
            this.txttotalTar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(757, 620);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "Tar.Wt";
            // 
            // txtTotalDiff
            // 
            this.txtTotalDiff.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalDiff.Location = new System.Drawing.Point(1058, 593);
            this.txtTotalDiff.Name = "txtTotalDiff";
            this.txtTotalDiff.Size = new System.Drawing.Size(94, 26);
            this.txtTotalDiff.TabIndex = 22;
            this.txtTotalDiff.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1058, 620);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(51, 18);
            this.label12.TabIndex = 21;
            this.label12.Text = "Diif Wt";
            // 
            // chckNumpad
            // 
            this.chckNumpad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chckNumpad.AutoSize = true;
            this.chckNumpad.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckNumpad.Location = new System.Drawing.Point(1151, 39);
            this.chckNumpad.Name = "chckNumpad";
            this.chckNumpad.Size = new System.Drawing.Size(95, 27);
            this.chckNumpad.TabIndex = 23;
            this.chckNumpad.Text = "Numpad";
            this.chckNumpad.UseVisualStyleBackColor = true;
            this.chckNumpad.CheckedChanged += new System.EventHandler(this.chckNumpad_CheckedChanged);
            // 
            // chckbatch
            // 
            this.chckbatch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chckbatch.AutoSize = true;
            this.chckbatch.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckbatch.Location = new System.Drawing.Point(1096, 651);
            this.chckbatch.Name = "chckbatch";
            this.chckbatch.Size = new System.Drawing.Size(143, 27);
            this.chckbatch.TabIndex = 24;
            this.chckbatch.Text = "Batch File Print";
            this.chckbatch.UseVisualStyleBackColor = true;
            // 
            // FrmGRNWeightCapture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1263, 684);
            this.Controls.Add(this.chckbatch);
            this.Controls.Add(this.chckNumpad);
            this.Controls.Add(this.txtTotalDiff);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txttotalTar);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtTotalNet);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtTotalActual);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.grWeight);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtBeamNo);
            this.Controls.Add(this.txtWeight);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.bntPrintBarcode);
            this.Name = "FrmGRNWeightCapture";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ACV Products Capture Weight";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmGRNWeightCapture_Load);
            this.grWeight.ResumeLayout(false);
            this.grWeight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridGRNWeight)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grWeight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDcDate;
        private System.Windows.Forms.TextBox txtSupplier;
        private System.Windows.Forms.TextBox txtGRNDate;
        private System.Windows.Forms.TextBox txtGRNNo;
        private System.Windows.Forms.DataGridView DataGridGRNWeight;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Button bntPrintBarcode;
        private System.Windows.Forms.TextBox txtBeamNo;
        private System.Windows.Forms.TextBox txtVechileNo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDCNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox txtTotalActual;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtTotalNet;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txttotalTar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtTotalDiff;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox chckNumpad;
        private TouchScreen touchScreen1;
        private System.Windows.Forms.CheckBox chckbatch;
    }
}