﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Globalization;
using CrystalDecisions.CrystalReports.Engine;
using System.Drawing.Printing;
using CrystalDecisions.Shared;

namespace ACVWeightCapture
{
    public partial class FrmGRNWeightCapture : Form
    {
        private TextBox focusedTextbox = null;
        public FrmGRNWeightCapture()
        {
            InitializeComponent();
            this.FormClosing += FrmGRNWeightCapture_FormClosing;
            touchScreen1.OnUserControlButtonClicked += new TouchScreen.ButtonClickedEventHandler(touchScreen1_OnUserControlButtonClicked);
        }

        private void touchScreen1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                if (focusedTextbox != null)
                {
                    if (b.Text == "Back")
                    {
                        if (focusedTextbox.Text.Length > 1)
                        {
                            focusedTextbox.Text = focusedTextbox.Text.Substring(0, focusedTextbox.Text.Length - 1);
                        }
                        else
                        {
                            focusedTextbox.Text = string.Empty;
                        }
                    }
                    else if (b.Text == "Clear")
                    {
                        focusedTextbox.Text = string.Empty;
                        focusedTextbox.Focus();
                    }
                    else if (b.Text == "TAB")
                    {
                        this.SelectNextControl(focusedTextbox, true, true, true, true);
                    }
                    else
                    {
                        if (MyGlobal.bTouch)
                            focusedTextbox.Text = b.Text;
                        else
                        {
                            MyGlobal.bTouch = false;
                            focusedTextbox.Text += b.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        SQLDBHelper db = new SQLDBHelper();
        private void FrmGRNWeightCapture_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    this.Hide();
                    FrmPendingGRN peniding = new FrmPendingGRN();
                    peniding.Show();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmPendingGRN grn = new FrmPendingGRN();
            grn.Show();
        }

        private void FrmGRNWeightCapture_Load(object sender, EventArgs e)
        {
            DataGridGRNWeight.EnableHeadersVisualStyles = false;
            DataGridGRNWeight.ColumnHeadersDefaultCellStyle.BackColor = Color.LightSeaGreen;
            grWeight.Height = this.Height - 100;
            DataGridGRNWeight.Height = this.Height - 250;
            grWeight.Width = this.Width - 250;
            DataGridGRNWeight.Width = this.Width - 250;

            txtGRNDate.Text = Convert.ToDateTime(FrmPendingGRN.GRNDate).ToString("dd-MMM-yyyy");
            txtGRNNo.Text = FrmPendingGRN.GRNNo;
            txtSupplier.Text = FrmPendingGRN.Supplier;
            txtVechileNo.Text = FrmPendingGRN.VechileNo;
            txtDCNo.Text = FrmPendingGRN.DCNo;
            txtDcDate.Text = Convert.ToDateTime(FrmPendingGRN.DCDate).ToString("dd-MMM-yyyy");

            int Uid = FrmPendingGRN.uid;
            SqlParameter[] para = { new SqlParameter("@Uid", Uid) };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetDetForPendingGRN", para);
            LoadTitle(dt);
            DataGridGRNWeight.Rows[0].Selected = true;
            chckbatch.Checked = true;
        }
        public void LoadTitle(DataTable dt)
        {
            DataGridGRNWeight.DataSource = null;
            DataGridGRNWeight.AutoGenerateColumns = false;
            DataGridGRNWeight.ColumnCount = 15;
            DataGridGRNWeight.Columns[0].Name = "Uid";
            DataGridGRNWeight.Columns[0].HeaderText = "Uid";
            DataGridGRNWeight.Columns[0].DataPropertyName = "Uid";
            DataGridGRNWeight.Columns[0].Visible = false;
            DataGridGRNWeight.Columns[1].Name = "SlNo";
            DataGridGRNWeight.Columns[1].HeaderText = "SlNo";
            DataGridGRNWeight.Columns[1].Width = 60;
            DataGridGRNWeight.Columns[2].Name = "SetNo";
            DataGridGRNWeight.Columns[2].HeaderText = "SetNo";
            DataGridGRNWeight.Columns[2].Width = 90;
            DataGridGRNWeight.Columns[3].Name = "Beam No";
            DataGridGRNWeight.Columns[3].HeaderText = "Beam No";
            DataGridGRNWeight.Columns[3].Width = 90;
            DataGridGRNWeight.Columns[4].Name = "Item";
            DataGridGRNWeight.Columns[4].HeaderText = "Item";
            DataGridGRNWeight.Columns[4].Width = 265;
            DataGridGRNWeight.Columns[5].Name = "ENDs";
            DataGridGRNWeight.Columns[5].HeaderText = "ENDs";
            DataGridGRNWeight.Columns[5].Width = 60;
            DataGridGRNWeight.Columns[6].Name = "Length";
            DataGridGRNWeight.Columns[6].HeaderText = "Length";
            DataGridGRNWeight.Columns[6].Width = 60;
            DataGridGRNWeight.Columns[7].Name = "Gross";
            DataGridGRNWeight.Columns[7].HeaderText = "Gross";
            DataGridGRNWeight.Columns[8].Name = "T.Wt";
            DataGridGRNWeight.Columns[8].HeaderText = "T.Wt";
            DataGridGRNWeight.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridGRNWeight.Columns[8].DefaultCellStyle.Format = "N3";
            DataGridGRNWeight.Columns[9].Name = "Net Wt";
            DataGridGRNWeight.Columns[9].HeaderText = "Net Wt";
            DataGridGRNWeight.Columns[9].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridGRNWeight.Columns[9].DefaultCellStyle.Format = "N3";
            DataGridGRNWeight.Columns[10].Name = "Actual Wt";
            DataGridGRNWeight.Columns[10].HeaderText = "Actual Wt";
            DataGridGRNWeight.Columns[10].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridGRNWeight.Columns[10].DefaultCellStyle.Format = "N3";
            DataGridGRNWeight.Columns[11].Name = "Barcode";
            DataGridGRNWeight.Columns[11].HeaderText = "Barcode";
            DataGridGRNWeight.Columns[11].Visible = false;
            DataGridGRNWeight.Columns[12].Name = "BeamUid";
            DataGridGRNWeight.Columns[12].HeaderText = "BeamUid";
            DataGridGRNWeight.Columns[12].Visible = false;
            DataGridGRNWeight.Columns[13].Name = "Count";
            DataGridGRNWeight.Columns[13].HeaderText = "Count";
            DataGridGRNWeight.Columns[13].Visible = false;
            DataGridGRNWeight.Columns[14].Name = "Diff.Wt";
            DataGridGRNWeight.Columns[14].HeaderText = "Diff.Wt";
            DataGridGRNWeight.Columns[14].Visible = true;
            DataGridGRNWeight.Columns[14].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            DataGridGRNWeight.Columns[14].DefaultCellStyle.Format = "N3";
            if (dt.Rows.Count != 0)
            {
                DataGridGRNWeight.Columns[0].DataPropertyName = "JorListUid";
                DataGridGRNWeight.Columns[2].DataPropertyName = "SetNo";
                DataGridGRNWeight.Columns[3].DataPropertyName = "GeneralName";
                DataGridGRNWeight.Columns[4].DataPropertyName = "ItemName";
                DataGridGRNWeight.Columns[5].DataPropertyName = "ENDs";
                DataGridGRNWeight.Columns[6].DataPropertyName = "QtyinMeters";
                DataGridGRNWeight.Columns[7].DataPropertyName = "QtyinWeight";
                DataGridGRNWeight.Columns[8].DataPropertyName = "TareWt";
                DataGridGRNWeight.Columns[9].DataPropertyName = "NetWt";
                DataGridGRNWeight.Columns[10].DataPropertyName = "Net";
                DataGridGRNWeight.Columns[11].DataPropertyName = "Barcode";
                DataGridGRNWeight.Columns[12].DataPropertyName = "BeamUid";
                DataGridGRNWeight.Columns[13].DataPropertyName = "cnt";
                DataGridGRNWeight.DataSource = dt;
            }
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataGridGRNWeight.Rows[i].Cells[1].Value = i + 1;
                DataGridGRNWeight.Rows[i].Cells[14].Value = 0.000;
            }
        }
        private void btnReset_Click(object sender, EventArgs e)
        {
            ClearControl();
            int Uid = FrmPendingGRN.uid;
            SqlParameter[] para = { new SqlParameter("@Uid", Uid) };
            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetDetForPendingGRN", para);
            LoadTitle(dt);
            DataGridGRNWeight.Rows[0].Selected = true;
            txtWeight.Focus();
        }
        public void ClearControl()
        {
            txtBeamNo.Text = string.Empty;
            txtWeight.Text = string.Empty;
        }

        private void bntPrintBarcode_Click(object sender, EventArgs e)
        {
            try
            {
                if (chckbatch.Checked == true)
                {
                    if (DataGridGRNWeight.SelectedRows.Count != 0)
                    {
                        DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_PrintBarcode");
                        FileStream fs = new FileStream(Application.StartupPath + "\\Barcode01.txt", FileMode.Truncate, FileAccess.Write);
                        fs.Close();
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            string Var = dt.Rows[i]["VarType"].ToString();
                            if (Var == "Static")
                            {
                                File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", dt.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                            }
                            else
                            {
                                int LineNUmber = Convert.ToInt32(dt.Rows[i]["LineNumber"].ToString());
                                int Index = DataGridGRNWeight.SelectedCells[0].RowIndex;
                                if (LineNUmber == 35)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + txtSupplier.Text + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 36)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridGRNWeight.Rows[Index].Cells[3].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 37)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridGRNWeight.Rows[Index].Cells[4].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 38)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridGRNWeight.Rows[Index].Cells[5].Value.ToString() + "\"";

                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 39)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridGRNWeight.Rows[Index].Cells[6].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 40)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridGRNWeight.Rows[Index].Cells[2].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 41)
                                {
                                    DateTime dte = Convert.ToDateTime(txtGRNDate.Text);
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + dte.ToString("dd.MM.yyyy") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 42)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridGRNWeight.Rows[Index].Cells[10].Value.ToString()).ToString("0.000") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 43)
                                {
                                    decimal Tare = Convert.ToDecimal(DataGridGRNWeight.Rows[Index].Cells[8].Value.ToString());
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + Tare.ToString("0.000") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 44)
                                {
                                    decimal Tare = Convert.ToDecimal(DataGridGRNWeight.Rows[Index].Cells[8].Value.ToString());
                                    decimal Weg = Convert.ToDecimal(DataGridGRNWeight.Rows[Index].Cells[10].Value.ToString());
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + (Weg - Tare).ToString("0.000") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 45)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridGRNWeight.Rows[Index].Cells[11].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 46)
                                {
                                    string text = dt.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridGRNWeight.Rows[Index].Cells[11].Value.ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\Barcode01.txt", text + Environment.NewLine);
                                }
                            }
                        }
                        int j = 2;
                        for (int i = 0; i < j; i++)
                        {
                            Process.Start(Application.StartupPath + "\\BPBeam.bat");
                        }
                        //Process.Start(Application.StartupPath + "\\BPBeam.bat");
                        //MessageBox.Show("Printed", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //Process.Start(Application.StartupPath + "\\BarPrint.bat");
                    }
                }
                else
                {
                    if (DataGridGRNWeight.SelectedRows.Count != 0)
                    {
                        int Index = DataGridGRNWeight.SelectedCells[0].RowIndex;
                        //Generate Barcode
                        KeepDynamic.Barcode.CrystalReport.BarCode br = new KeepDynamic.Barcode.CrystalReport.BarCode();
                        br.SymbologyType = KeepDynamic.Barcode.CrystalReport.SymbologyType.Code128;
                        br.CodeText = DataGridGRNWeight.Rows[Index].Cells[11].Value.ToString();
                        br.DisplayCodeText = false;
                        byte[] bt = br.drawBarcodeAsBytes();
                        //Net Weight
                        decimal Tare = Convert.ToDecimal(DataGridGRNWeight.Rows[Index].Cells[8].Value.ToString());
                        decimal Weg = Convert.ToDecimal(DataGridGRNWeight.Rows[Index].Cells[10].Value.ToString());
                        DateTime dtgrn = Convert.ToDateTime(txtGRNDate.Text);

                        DataTable dt = new DataTable();
                        dt.Columns.Add("Barcode", typeof(byte[]));
                        dt.Columns.Add("Sizer", typeof(string));
                        dt.Columns.Add("BeamNo", typeof(string));
                        dt.Columns.Add("Count", typeof(string));
                        dt.Columns.Add("Ends", typeof(string));
                        dt.Columns.Add("Mtrs", typeof(string));
                        dt.Columns.Add("SetNo", typeof(string));
                        dt.Columns.Add("GWt", typeof(decimal));
                        dt.Columns.Add("TWt", typeof(decimal));
                        dt.Columns.Add("NWt", typeof(decimal));
                        dt.Columns.Add("Date", typeof(DateTime));
                        dt.Columns.Add("BarText", typeof(string));
                        DataRow row = dt.NewRow();
                        row["Barcode"] = bt;
                        row["Sizer"] = txtSupplier.Text;
                        row["BeamNo"] = DataGridGRNWeight.Rows[Index].Cells[3].Value.ToString();
                        row["Count"] = DataGridGRNWeight.Rows[Index].Cells[4].Value.ToString();
                        row["Ends"] = DataGridGRNWeight.Rows[Index].Cells[5].Value.ToString();
                        row["Mtrs"] = DataGridGRNWeight.Rows[Index].Cells[6].Value.ToString();
                        row["SetNo"] = DataGridGRNWeight.Rows[Index].Cells[2].Value.ToString();
                        row["GWt"] = Convert.ToDecimal(DataGridGRNWeight.Rows[Index].Cells[10].Value.ToString()).ToString("0.000");
                        row["TWt"] = Convert.ToDecimal(DataGridGRNWeight.Rows[Index].Cells[8].Value.ToString()).ToString("0.000");
                        row["NWt"] = (Weg - Tare).ToString("0.000");
                        row["Date"] = dtgrn;
                        row["BarText"] = DataGridGRNWeight.Rows[Index].Cells[11].Value.ToString();
                        dt.Rows.Add(row);
                        ReportDocument doc = new ReportDocument();
                        doc.Load(Application.StartupPath + "\\CryBeamBarcode.rpt");
                        doc.SetDataSource(dt);
                        PrintDocument pDoc = new PrintDocument();
                        PrintLayoutSettings PrintLayout = new PrintLayoutSettings();
                        PrinterSettings printerSettings = new PrinterSettings();
                        printerSettings.PrinterName = pDoc.PrinterSettings.PrinterName;
                        PageSettings pSettings = new PageSettings(printerSettings);
                        pSettings.PrinterSettings.Copies = 1;
                        doc.PrintOptions.DissociatePageSizeAndPrinterPaperSize = true;
                        doc.PrintOptions.PrinterDuplex = PrinterDuplex.Simplex;
                        doc.PrintToPrinter(printerSettings, pSettings, false, PrintLayout);
                    }
                    else
                    {
                        MessageBox.Show("Select Beam Number to Print Barcode ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

        }

        private void DataGridGRNWeight_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                int index = DataGridGRNWeight.SelectedCells[0].RowIndex;
                txtBeamNo.Text = DataGridGRNWeight.Rows[index].Cells[3].Value.ToString();
                txtWeight.Text = string.Empty;
                txtWeight.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridGRNWeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.Equals(Keys.Up) || e.KeyCode.Equals(Keys.Down))
            {
                MoveUpDown(e.KeyCode == Keys.Up);
            }
            e.Handled = true;
        }
        private void MoveUpDown(bool goUp)
        {
            try
            {
                int currentRowindex = DataGridGRNWeight.SelectedCells[0].OwningRow.Index;
                int newRowIndex = currentRowindex + (goUp ? -1 : 1);
                if (newRowIndex > -1 && newRowIndex < DataGridGRNWeight.Rows.Count)
                {
                    DataGridGRNWeight.ClearSelection();
                    DataGridGRNWeight.Rows[newRowIndex].Selected = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Error");
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataGridGRNWeight.SelectedRows.Count != 0)
                {
                    int Index = DataGridGRNWeight.SelectedCells[0].RowIndex;
                    int JorListUid = Convert.ToInt32(DataGridGRNWeight.Rows[Index].Cells[0].Value.ToString());
                    decimal ActualWait = Convert.ToDecimal(txtWeight.Text);
                    int BeamUid = Convert.ToInt32(DataGridGRNWeight.Rows[Index].Cells[12].Value.ToString());
                    int JorUid = FrmPendingGRN.uid;
                    SqlParameter[] para = {
                        new SqlParameter("@Uid",JorListUid),
                        new SqlParameter("@ActualWeight",ActualWait),
                        new SqlParameter("@BeamUid",BeamUid),
                        new SqlParameter("@JorUid",JorUid)
                    };
                    int i = db.ExecuteQuery(CommandType.StoredProcedure, "SP_UpdateActualWeight", para);
                    DataGridGRNWeight.Rows[Index].Cells[10].Value = ActualWait;
                    txtWeight.Text = string.Empty;
                    txtBeamNo.Text = string.Empty;
                    bntPrintBarcode_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("Select Beam  Number ", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtWeight_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    btnSave_Click(sender, e);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                return;
            }

        }

        private void DataGridGRNWeight_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            decimal TotalTareWeight = 0;
            decimal TotalNetWeight = 0;
            decimal TotalActualWeight = 0;
            decimal TotalDiffWeight = 0;
            for (int i = 0; i < DataGridGRNWeight.Rows.Count; i++)
            {

                decimal TareWeight = Convert.ToDecimal(DataGridGRNWeight.Rows[i].Cells[8].Value.ToString());
                decimal NetWeight = Convert.ToDecimal(DataGridGRNWeight.Rows[i].Cells[9].Value.ToString());
                decimal ActualWeight = Convert.ToDecimal(DataGridGRNWeight.Rows[i].Cells[10].Value.ToString());
                if (ActualWeight != 0)
                {
                    TotalActualWeight += ActualWeight;
                    TotalNetWeight += NetWeight;
                    TotalTareWeight += TareWeight;
                    TotalDiffWeight += NetWeight - (ActualWeight - TareWeight);
                    DataGridGRNWeight.Rows[i].Cells[14].Value = (ActualWeight - TareWeight) - NetWeight;
                }
                else
                {
                    TotalActualWeight += ActualWeight;
                    TotalNetWeight += NetWeight;
                    TotalTareWeight += TareWeight;
                }
            }
            txtTotalActual.Text = TotalActualWeight.ToString();
            txttotalTar.Text = TotalTareWeight.ToString();
            txtTotalNet.Text = TotalNetWeight.ToString();
            txtTotalDiff.Text = TotalDiffWeight.ToString();
        }

        private void chckNumpad_CheckedChanged(object sender, EventArgs e)
        {
            if (chckNumpad.Checked == true)
            {
                touchScreen1.Visible = true;
                txtWeight.Focus();
            }
            else
            {
                touchScreen1.Visible = false;
            }
        }

        private void txtWeight_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }
    }
}
