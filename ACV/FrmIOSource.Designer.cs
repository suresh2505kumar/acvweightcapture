﻿namespace ACVWeightCapture
{
    partial class FrmIOSource
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnInSource = new System.Windows.Forms.Button();
            this.btnOutSource = new System.Windows.Forms.Button();
            this.panelMain = new System.Windows.Forms.Panel();
            this.btnBeamReceipt = new System.Windows.Forms.Button();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnInSource
            // 
            this.btnInSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnInSource.BackColor = System.Drawing.Color.Chartreuse;
            this.btnInSource.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInSource.Location = new System.Drawing.Point(102, 103);
            this.btnInSource.Name = "btnInSource";
            this.btnInSource.Size = new System.Drawing.Size(200, 137);
            this.btnInSource.TabIndex = 2;
            this.btnInSource.Text = "In Source";
            this.btnInSource.UseVisualStyleBackColor = false;
            this.btnInSource.Click += new System.EventHandler(this.btnInSource_Click);
            // 
            // btnOutSource
            // 
            this.btnOutSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnOutSource.BackColor = System.Drawing.Color.Chartreuse;
            this.btnOutSource.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOutSource.Location = new System.Drawing.Point(308, 103);
            this.btnOutSource.Name = "btnOutSource";
            this.btnOutSource.Size = new System.Drawing.Size(200, 137);
            this.btnOutSource.TabIndex = 3;
            this.btnOutSource.Text = "Out Source";
            this.btnOutSource.UseVisualStyleBackColor = false;
            this.btnOutSource.Click += new System.EventHandler(this.btnOutSource_Click);
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.btnBeamReceipt);
            this.panelMain.Controls.Add(this.btnOutSource);
            this.panelMain.Controls.Add(this.btnInSource);
            this.panelMain.Location = new System.Drawing.Point(42, 56);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(832, 361);
            this.panelMain.TabIndex = 4;
            // 
            // btnBeamReceipt
            // 
            this.btnBeamReceipt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.btnBeamReceipt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btnBeamReceipt.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBeamReceipt.Location = new System.Drawing.Point(514, 103);
            this.btnBeamReceipt.Name = "btnBeamReceipt";
            this.btnBeamReceipt.Size = new System.Drawing.Size(200, 137);
            this.btnBeamReceipt.TabIndex = 4;
            this.btnBeamReceipt.Text = "Beam Receipt Entry";
            this.btnBeamReceipt.UseVisualStyleBackColor = false;
            this.btnBeamReceipt.Click += new System.EventHandler(this.btnBeamReceipt_Click);
            // 
            // FrmIOSource
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(911, 465);
            this.Controls.Add(this.panelMain);
            this.Name = "FrmIOSource";
            this.Text = "In Source or Out Source";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmIOSource_Load);
            this.panelMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnInSource;
        private System.Windows.Forms.Button btnOutSource;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Button btnBeamReceipt;
    }
}