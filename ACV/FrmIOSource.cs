﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ACVWeightCapture
{
    public partial class FrmIOSource : Form
    {
        public FrmIOSource()
        {
            InitializeComponent();
            this.FormClosing += FrmIOSource_FormClosing; ;
        }

        private void FrmIOSource_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                DialogResult result = MessageBox.Show("Do you really want to exit?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                if (result == DialogResult.Yes)
                {
                    Environment.Exit(0);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnOutSource_Click(object sender, EventArgs e)
        {
            try
            {
                FrmPendingGRN grn = new FrmPendingGRN();
                grn.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmIOSource_Load(object sender, EventArgs e)
        {
            panelMain.Location = new Point(ClientSize.Width / 2 - panelMain.Size.Width / 2, ClientSize.Height / 2 - panelMain.Size.Height / 2);
            panelMain.Anchor = AnchorStyles.None;
        }

        private void btnInSource_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                FrmWarpWeight weight = new FrmWarpWeight();
                weight.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBeamReceipt_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmWarbBooking rece = new FrmWarbBooking();
            rece.StartPosition = FormStartPosition.CenterScreen;
            rece.Show();
        }
    }
}
