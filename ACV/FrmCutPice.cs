﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ACVWeightCapture
{
    public partial class FrmCutPice : Form
    {
        private TextBox focusedTextbox = null;
        public FrmCutPice()
        {
            InitializeComponent();
            this.FormClosing += FrmCutPice_FormClosing;
            touchScreen1.OnUserControlButtonClicked += new TouchScreen.ButtonClickedEventHandler(touchScreen1_OnUserControlButtonClicked);
        }
        string tag;
        int QCDuid;
        DataTable dtBarcode = new DataTable();
        SQLDBHelper db = new SQLDBHelper();
        BindingSource bsQC = new BindingSource();
        private void touchScreen1_OnUserControlButtonClicked(object sender, EventArgs e)
        {
            try
            {
                Button b = (Button)sender;
                if (focusedTextbox != null)
                {
                    if (b.Text == "Back")
                    {
                        if (focusedTextbox.Text.Length > 1)
                        {
                            focusedTextbox.Text = focusedTextbox.Text.Substring(0, focusedTextbox.Text.Length - 1);
                        }
                        else
                        {
                            focusedTextbox.Text = string.Empty;
                        }
                    }
                    else if (b.Text == "Clear")
                    {
                        focusedTextbox.Text = string.Empty;
                        focusedTextbox.Focus();
                    }
                    else if (b.Text == "TAB")
                    {
                        this.SelectNextControl(focusedTextbox, true, true, true, true);
                    }
                    else if (b.Text == "Enter")
                    {
                        EnterKeypress(focusedTextbox.Name);
                    }
                    else
                    {
                        if (MyGlobal.bTouch)
                            focusedTextbox.Text = b.Text;
                        else
                        {
                            MyGlobal.bTouch = false;
                            focusedTextbox.Text += b.Text;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void EnterKeypress(string name)
        {
            if (name == "txtMender")
            {
                if (txtMender.Text != string.Empty)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@TypeM_Uid",28),
                        new SqlParameter("@Active",1),
                        new SqlParameter("@Code",txtMender.Text)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                    if (dt.Rows.Count != 0)
                    {
                        lblMendorName.Text = dt.Rows[0]["f2"].ToString();
                        txtMender.Tag = dt.Rows[0]["Uid"].ToString();
                        txtbarcode.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Invalid mendor Code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtMender.Focus();
                        return;
                    }

                }
                else
                {
                    MessageBox.Show("Enter mendor Code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else if (name == "txtBarcode")
            {
                if (tag == "QCPass")
                {
                    if (txtMender.Text != string.Empty)
                    {
                        SqlParameter[] para = {
                        new SqlParameter("@Barcode",txtbarcode.Text)
                    };
                        DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortNoRollNo", para);
                        if (dt.Rows.Count != 0)
                        {
                            lblSortNo.Text = dt.Rows[0]["SortNo"].ToString();
                            lblSortNo.Tag = dt.Rows[0]["NetWt"].ToString();
                            lblWtPMtr.Text = dt.Rows[0]["WTMtr"].ToString();
                            txtbarcode.Tag = dt.Rows[0]["id"].ToString();
                            txtMeter.Tag = dt.Rows[0]["GTLEN"].ToString();
                            txtWeight.Tag = dt.Rows[0]["WTMtr"].ToString();
                            cmbNoofPices.SelectedIndex = 0;
                            cmbNoofPices.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Invalid Barcode Code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtbarcode.Focus();
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter Barcode Code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else
                {

                }

            }
            else if (name == "txtWeight")
            {
                cmbQc.Focus();
            }
        }

        private void FrmCutPice_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (e.CloseReason == CloseReason.UserClosing)
                {
                    this.Hide();
                    FrmQc qc = new FrmQc();
                    qc.Show();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void FrmCutPice_Load(object sender, EventArgs e)
        {
            grFront.Width = this.Width - 30;
            grCutpice.Width = this.Width - 30;
            grCutpice.Height = this.Height - 70;
            grFront.Height = this.Height - 70;
            DataGridQC.Height = grFront.Height - 60;
            DataGridQC.Width = grFront.Width - 220;
            txtSearch.Width = DataGridQC.Width;
            panelMain.Location = new Point(ClientSize.Width / 2 - panelMain.Size.Width / 2, ClientSize.Height / 2 - panelMain.Size.Height / 2);
            panelMain.Anchor = AnchorStyles.None;
            grCutpice.Visible = false;
            grFront.Visible = true;
            txtSearch.Focus();
            cmbQCSt.SelectedIndex = 0;
            GetQC(0);
        }

        private void PrintBarcode()
        {
            try
            {
                FileStream fs = new FileStream(Application.StartupPath + "\\QCBarcode.txt", FileMode.Truncate, FileAccess.Write);
                fs.Close();
                for (int i = 0; i < dtBarcode.Rows.Count; i++)
                {
                    string Var = dtBarcode.Rows[i]["VarType"].ToString();
                    if (Var == "Static")
                    {
                        File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", dtBarcode.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                    }
                    else
                    {
                        int LineNUmber = Convert.ToInt32(dtBarcode.Rows[i]["LineNumber"].ToString());
                        if (LineNUmber == 10)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + cmbBarcodeNoofPices.Text + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 12)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + cmbBarcodeNoofPices.Text + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 19)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + lblSortNo.Text + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 20)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(txtMeter.Text).ToString("0.000") + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 21)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(txtWeight.Text).ToString("0.000") + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 24)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDateTime(dtpDate.Text).ToString("dd.MM.yyyy") + "/" + cmbQc.Text + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                        else if (LineNUmber == 27)
                        {
                            string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + lblMendorName.Text + "\"";
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                        }
                    }
                }
                Process.Start(Application.StartupPath + "\\BPQc.bat");
                //MessageBox.Show("Printed", "Infromation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //Process.Start(Application.StartupPath + "\\BarPrint.bat");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void chckNumpad_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                //DialogResult result = MessageBox.Show("Do you really back to the application?", "Closing", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
                //if (result == DialogResult.Yes)
                //{
                grFront.Visible = true;
                grCutpice.Visible = false;
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtbarcode_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
            if(txtMender.Text == string.Empty)
            {
                MessageBox.Show("Enter Mendor Code", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtMender.Focus();
            }
        }

        private void txtMeter_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void txtWeight_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void cmbNoofPices_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tag == "QCPass")
                {
                    if (cmbNoofPices.SelectedIndex != -1)
                    {
                        if (txtbarcode.Text != string.Empty)
                        {
                            cmbBarcodeNoofPices.Items.Clear();
                            if (cmbNoofPices.SelectedIndex != -1)
                            {
                                int cnt = Convert.ToInt32(cmbNoofPices.Text);
                                for (int i = 0; i < cnt; i++)
                                {
                                    cmbBarcodeNoofPices.Items.Add(txtbarcode.Text + "-" + (i + 1).ToString("D1"));
                                }
                            }
                            SqlParameter[] para = { new SqlParameter("@TotalMeters", txtbarcode.Text) };
                            DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortDetailId", para);
                            txtbarcode.Tag = dt.Rows[0]["Id"].ToString();
                            cmbBarcodeNoofPices.SelectedIndex = 0;
                            txtMeter.Focus();
                            //if (txtMeter.Tag.ToString() == "0.00")
                            //{
                            //    txtWeight.Focus();
                            //}
                            //else
                            //{
                            //    txtMeter.Focus();
                            //}
                        }
                        else
                        {
                            MessageBox.Show("Enter Barcode", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtbarcode.Focus();
                        }
                    }
                }
                else
                {
                    cmbBarcodeNoofPices.Items.Clear();
                    if (cmbNoofPices.SelectedIndex != -1)
                    {
                        int cnt = Convert.ToInt32(cmbNoofPices.Text);
                        for (int i = 0; i < cnt; i++)
                        {
                            cmbBarcodeNoofPices.Items.Add(txtbarcode.Text + (i + 1).ToString("D1"));
                        }
                        cmbBarcodeNoofPices.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                bool entryFoundChck = false;
                if(cmbBarcodeNoofPices.SelectedIndex == -1)
                {
                    MessageBox.Show("Select pices Barcode", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbBarcodeNoofPices.Focus();
                    return;
                }
                decimal val = 0;
                foreach (DataGridViewRow row1 in DataGridCutPices.Rows)
                {
                    for (int i = 0; i < DataGridCutPices.Rows.Count - 1; i++)
                    {
                        if (val == 0)
                        {
                            val = Convert.ToDecimal(row1.Cells[3].Value.ToString());
                        }
                        else
                        {
                            val += Convert.ToDecimal(row1.Cells[3].Value.ToString());
                        }
                    }
                    val += Convert.ToDecimal(txtWeight.Text);
                    decimal val2 = Convert.ToDecimal(txtWeight.Tag.ToString()) + 10;
                    if (val <= val2)
                    {
                        entryFoundChck = false;
                        break;
                    }
                    else
                    {
                        entryFoundChck = true;
                        break;
                    }
                }
                if (!entryFoundChck)
                {
                    string barcode = cmbBarcodeNoofPices.Text;
                    if (txtWeight.Text != string.Empty && cmbBarcodeNoofPices.Text != string.Empty || txtMeter.Text != string.Empty)
                    {
                        bool entryFound = false;
                        foreach (DataGridViewRow row1 in DataGridCutPices.Rows)
                        {
                            object val2 = row1.Cells[1].Value;
                            if (val2 != null && val2.ToString() == cmbBarcodeNoofPices.Text)
                            {
                                MessageBox.Show("Entry already exist", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                entryFound = true;
                                break;
                            }
                        }
                        if (!entryFound)
                        {
                            decimal Weight = 0;
                            if (txtNetWeight.Text == string.Empty)
                            {
                                txtNetWeight.Text = "0";
                            }
                            if(lblWtPMtr.Text == "-")
                            {
                                lblWtPMtr.Text = "0";
                            }
                            decimal wtprmtr = Convert.ToDecimal(lblWtPMtr.Text);
                            Weight = Convert.ToDecimal(txtNetWeight.Text);
                            decimal calcuwtmtr = ((Convert.ToDecimal(txtWeight.Text) / Convert.ToDecimal(txtMeter.Text)) * 1000) ;
                            //MessageBox.Show(calcuwtmtr.ToString());
                            if(calcuwtmtr <= wtprmtr -10)
                            {
                                DialogResult result = MessageBox.Show("Weight Per Meter < " + calcuwtmtr.ToString("0.000") + " Do You want Continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);
                                if(result == DialogResult.Yes)
                                {

                                }
                                else
                                {
                                    txtMeter.Focus();
                                    return;
                                }
                            }
                            if (calcuwtmtr >= wtprmtr + 10)
                            {
                                DialogResult result = MessageBox.Show("Weight Per Meter > " + calcuwtmtr.ToString("0.000") + " Do You want Continue ?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2);                                if (result == DialogResult.Yes)
                                    if (result == DialogResult.Yes)
                                    {

                                    }
                                    else
                                {
                                    txtMeter.Focus();
                                    return;
                                }
                            }
                            DataGridViewRow row = (DataGridViewRow)DataGridCutPices.Rows[0].Clone();
                            row.Cells[0].Value = DataGridCutPices.Rows.Count;
                            row.Cells[1].Value = cmbBarcodeNoofPices.Text;
                            row.Cells[2].Value = txtMeter.Text;
                            row.Cells[3].Value = txtWeight.Text;
                            row.Cells[4].Value = txtbarcode.Text;
                            row.Cells[5].Value = cmbQc.Text;
                            row.Cells[6].Value = ((Convert.ToDecimal(txtWeight.Text) / Convert.ToDecimal(txtMeter.Text)) * 1000).ToString("0.000");
                            DataGridCutPices.Rows.Add(row);
                            txtNetWeight.Text = (Weight + Convert.ToDecimal(txtWeight.Text)).ToString();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter Barcode,Weight and Meter", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbBarcodeNoofPices.Focus();
                    }
                    PrintBarcode();
                    txtMeter.Text = string.Empty;
                    txtWeight.Text = string.Empty;
                    cmbBarcodeNoofPices.Items.Remove(barcode);
                    if (cmbBarcodeNoofPices.Items.Count != 0)
                    {
                        cmbBarcodeNoofPices.SelectedIndex = 0;
                    }
                    txtMeter.Focus();
                }
                else
                {
                    MessageBox.Show("Check the Weight", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtWeight.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void LoadWeight()
        {
            try
            {
                decimal Weight = 0;
                for (int i = 0; i < DataGridCutPices.Rows.Count - 1; i++)
                {
                    Weight += Convert.ToDecimal(DataGridCutPices.Rows[i].Cells[3].Value);
                }
                txtNetWeight.Text = Weight.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void grCutpice_Enter(object sender, EventArgs e)
        {

        }

        protected void LoadTitle()
        {
            try
            {
                DataGridCutPices.DataSource = null;
                DataGridCutPices.AutoGenerateColumns = false;
                DataGridCutPices.ColumnCount = 7;
                DataGridCutPices.Columns[0].Name = "SlNo";
                DataGridCutPices.Columns[0].Name = "SlNo";

                DataGridCutPices.Columns[1].Name = "QCBarcode";
                DataGridCutPices.Columns[1].Name = "QCBarcode";
                DataGridCutPices.Columns[1].Width = 170;

                DataGridCutPices.Columns[2].Name = "Length";
                DataGridCutPices.Columns[2].Name = "Length";
                DataGridCutPices.Columns[2].Width = 150;

                DataGridCutPices.Columns[3].Name = "Weight";
                DataGridCutPices.Columns[3].Name = "Weight";
                DataGridCutPices.Columns[3].Width = 150;
                DataGridCutPices.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                DataGridCutPices.Columns[4].Name = "Barcode";
                DataGridCutPices.Columns[4].Name = "Barcode";
                DataGridCutPices.Columns[4].Visible = false;
                DataGridCutPices.Columns[5].Name = "Status";
                DataGridCutPices.Columns[5].Name = "Status";
                DataGridCutPices.Columns[5].Width = 120;
                DataGridCutPices.Columns[6].Name = "Status";
                DataGridCutPices.Columns[6].Name = "Status";
                DataGridCutPices.Columns[6].Visible = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void DataGridCutPices_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            for (int i = 0; i < DataGridCutPices.Rows.Count - 1; i++)
            {
                DataGridCutPices.Rows[i].Cells[0].Value = i + 1;
            }
        }

        private void bntPrintBarcode_Click(object sender, EventArgs e)
        {
            try
            {
                if (DataGridCutPices.Rows.Count != 0)
                {
                    int Index = DataGridCutPices.SelectedCells[0].RowIndex;
                    FileStream fs = new FileStream(Application.StartupPath + "\\QCBarcode.txt", FileMode.Truncate, FileAccess.Write);
                    fs.Close();
                    for (int i = 0; i < dtBarcode.Rows.Count; i++)
                    {
                        string Var = dtBarcode.Rows[i]["VarType"].ToString();
                        if (Var == "Static")
                        {
                            File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", dtBarcode.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                        }
                        else
                        {
                            int LineNUmber = Convert.ToInt32(dtBarcode.Rows[i]["LineNumber"].ToString());
                            if (LineNUmber == 10)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridCutPices.Rows[Index].Cells[1].Value + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 12)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + DataGridCutPices.Rows[Index].Cells[1].Value + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 19)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + lblSortNo.Text + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 20)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridCutPices.Rows[Index].Cells[2].Value).ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 21)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(DataGridCutPices.Rows[Index].Cells[3].Value).ToString("0.000") + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 24)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDateTime(dtpDate.Text).ToString("dd.MM.yyyy") + "/" + DataGridCutPices.Rows[Index].Cells[5].Value.ToString() + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                            else if (LineNUmber == 27)
                            {
                                string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + lblMendorName.Text + "\"";
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                            }
                        }
                    }
                    Process.Start(Application.StartupPath + "\\BPQc.bat");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int ShiftId = 0;
                DateTime dte = DateTime.Now.Date;
                TimeSpan tme = DateTime.Now.TimeOfDay;
                int t1 = tme.Hours;
                if (t1 >= 8 && t1 < 16)
                {
                    ShiftId = 984;
                }
                else if (t1 >= 16 && t1 <= 24)
                {
                    ShiftId = 985;
                }
                else if (t1 >= 0 && t1 < 8)
                {
                    ShiftId = 986;
                }
                if (txtNetWeight.Text != string.Empty)
                {
                    if (DataGridCutPices.Rows.Count > 1)
                    {
                        SqlParameter[] para = {
                                new SqlParameter("@QCBarcode",DBNull.Value),
                                new SqlParameter("@QCDate",DateTime.Now),
                                new SqlParameter("@TotalWeight",Convert.ToDecimal(txtNetWeight.Text)),
                                new SqlParameter("@RefUid",txtbarcode.Tag),
                                new SqlParameter("@Refbarcode",txtbarcode.Text),
                                new SqlParameter("@ReturnMessage",SqlDbType.Int),
                                new SqlParameter("@MentorName",lblMendorName.Text),
                                new SqlParameter("@ShiftId",ShiftId)
                            };
                        para[5].Direction = ParameterDirection.Output;
                        int id = db.ExecuteQuery(CommandType.StoredProcedure, "SP_QcM", para, 5);

                        if (id != -1)
                        {
                            for (int i = 0; i < DataGridCutPices.Rows.Count - 1; i++)
                            {
                                string mendingsts = DataGridCutPices.Rows[i].Cells[5].Value.ToString();
                                SqlParameter[] paraDet = {
                                    new SqlParameter("@QCUid",id),
                                    new SqlParameter("@QCBarcode",DataGridCutPices.Rows[i].Cells[1].Value.ToString()),
                                    new SqlParameter("@Meters",Convert.ToDecimal(DataGridCutPices.Rows[i].Cells[2].Value.ToString())),
                                    new SqlParameter("@Weight",Convert.ToDecimal(DataGridCutPices.Rows[i].Cells[3].Value.ToString())),
                                    new SqlParameter("@RefBarcode",DataGridCutPices.Rows[i].Cells[4].Value.ToString()),
                                    new SqlParameter("@QCSts",mendingsts),
                                    new SqlParameter("@QcWtMtr",DataGridCutPices.Rows[i].Cells[6].Value.ToString()),
                                };
                                db.ExecuteQuery(CommandType.StoredProcedure, "SP_QCDet", paraDet);
                            }
                            if (tag == "Mending")
                            {
                                SqlParameter[] paraUpdateQc = { new SqlParameter("@QCBar", txtbarcode.Text) };
                                string Query = "Update QCDet Set QCSts='MQC Pass' Where QcBarcode =@QCBar";
                                db.ExecuteQuery(CommandType.Text, Query, paraUpdateQc);
                                tag = "QCPass";
                            }
                        }
                        else
                        {
                            MessageBox.Show("Roll number already exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }

                else
                {
                    MessageBox.Show("Enter net Weight", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtNetWeight.Focus();
                }
                ClearControl();
                txtMender.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected void ClearControl()
        {
            txtbarcode.Text = string.Empty;
            txtWeight.Text = string.Empty;
            txtMeter.Text = string.Empty;
            cmbNoofPices.SelectedIndex = -1;
            cmbBarcodeNoofPices.Items.Clear();
            lblSortNo.Text = string.Empty;
            txtNetWeight.Text = string.Empty;
            DataGridCutPices.Rows.Clear();
            txtMender.Text = string.Empty;
            lblMendorName.Text = string.Empty;
            lblWtPMtr.Text = string.Empty;
            txtMender.Focus();
        }

        private void txtbarcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Barcode",txtbarcode.Text)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortNoRollNo", para);
                    lblSortNo.Text = dt.Rows[0]["SortNo"].ToString();
                    lblSortNo.Tag = dt.Rows[0]["NetWt"].ToString();
                    lblWtPMtr.Text = dt.Rows[0]["WTMtr"].ToString();
                    txtbarcode.Tag = dt.Rows[0]["id"].ToString();
                    txtMeter.Tag = dt.Rows[0]["GTLEN"].ToString();
                    txtWeight.Tag = dt.Rows[0]["WTMtr"].ToString();
                    cmbNoofPices.SelectedIndex = 0;
                    cmbNoofPices.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtMeter_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    txtWeight.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtWeight_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    cmbQc.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ClearControl();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@TypeM_Uid",28),
                        new SqlParameter("@Active",1),
                        new SqlParameter("@Code",txtMender.Text)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                    lblMendorName.Text = dt.Rows[0]["f2"].ToString();
                    txtMender.Tag = dt.Rows[0]["Uid"].ToString();
                    txtbarcode.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            focusedTextbox = (TextBox)sender;
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtMender.Text != string.Empty)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@TypeM_Uid",28),
                        new SqlParameter("@Active",1),
                        new SqlParameter("@Code",txtMender.Text)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetGeneralM", para);
                    lblMendorName.Text = dt.Rows[0]["f2"].ToString();
                    txtMender.Tag = dt.Rows[0]["Uid"].ToString();
                    txtbarcode.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtbarcode_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtbarcode.Text != string.Empty)
                {
                    SqlParameter[] para = {
                        new SqlParameter("@Barcode",txtbarcode.Text)
                    };
                    DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_GetSortNoRollNo", para);
                    lblSortNo.Text = dt.Rows[0]["SortNo"].ToString();
                    lblWtPMtr.Text = dt.Rows[0]["WTMtr"].ToString();
                    txtbarcode.Tag = dt.Rows[0]["id"].ToString();
                    txtMeter.Tag = dt.Rows[0]["GTLEN"].ToString();
                    txtWeight.Tag = dt.Rows[0]["WTMtr"].ToString();
                    cmbNoofPices.SelectedIndex = 0;
                    cmbNoofPices.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbQc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbQc.SelectedIndex != -1)
            {
                btnOK.Focus();
            }
        }

        private void cmbBarcodeNoofPices_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbQc.SelectedIndex = 0;
        }

        private void btnQc_Click(object sender, EventArgs e)
        {
            try
            {
                tag = "QCPass";
                LoadTitle();
                dtBarcode = db.GetData(CommandType.StoredProcedure, "SP_GetQCBarcode");
                grCutpice.Visible = true;
                grFront.Visible = false;
                txtMender.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();
                FrmQc qc = new FrmQc();
                qc.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        protected DataSet GetQC(int Uid)
        {
            DataSet dt = new DataSet();
            try
            {
                SqlParameter[] para = { new SqlParameter("@QCType", cmbQCSt.Text), new SqlParameter("@Date", Convert.ToDateTime(dtpfilterDate.Text)) };
                DataSet ds = db.GetMultipleData(CommandType.StoredProcedure, "SP_GetQC", para);
                LoadQCGrid(ds);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        protected void LoadQCGrid(DataSet ds)
        {
            try
            {
                DataTable dt = ds.Tables[0];
                bsQC.DataSource = dt;
                DataGridQC.DataSource = null;
                DataGridQC.AutoGenerateColumns = false;
                DataGridQC.ColumnCount = 12;
                DataGridQC.Columns[0].Name = "QcUid";
                DataGridQC.Columns[0].HeaderText = "QcUid";
                DataGridQC.Columns[0].DataPropertyName = "QcUid";
                DataGridQC.Columns[0].Visible = false;

                DataGridQC.Columns[1].Name = "SLNO";
                DataGridQC.Columns[1].HeaderText = "SLNO";
                DataGridQC.Columns[1].Width = 50;

                DataGridQC.Columns[2].Name = "Time";
                DataGridQC.Columns[2].HeaderText = "Time";
                DataGridQC.Columns[2].DataPropertyName = "Tme";
                DataGridQC.Columns[2].Width = 80;

                DataGridQC.Columns[3].Name = "LoomNo";
                DataGridQC.Columns[3].HeaderText = "LoomNo";
                DataGridQC.Columns[3].DataPropertyName = "LoomNo";

                DataGridQC.Columns[4].Name = "QCBarcode";
                DataGridQC.Columns[4].HeaderText = "QC Barcode";
                DataGridQC.Columns[4].DataPropertyName = "QCBarcode";

                DataGridQC.Columns[5].Name = "SortNo";
                DataGridQC.Columns[5].HeaderText = "SortNo";
                DataGridQC.Columns[5].DataPropertyName = "SortNo";
                DataGridQC.Columns[5].Width = 210;

                DataGridQC.Columns[6].Name = "MentorName";
                DataGridQC.Columns[6].HeaderText = "Mendor Name";
                DataGridQC.Columns[6].DataPropertyName = "MentorName";
                DataGridQC.Columns[6].Width = 170;

                DataGridQC.Columns[7].Name = "Meters";
                DataGridQC.Columns[7].HeaderText = "Meters";
                DataGridQC.Columns[7].DataPropertyName = "Meters";

                DataGridQC.Columns[8].Name = "TotalWeight";
                DataGridQC.Columns[8].HeaderText = "TotalWeight";
                DataGridQC.Columns[8].DataPropertyName = "Weight";
                DataGridQC.Columns[8].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

                DataGridQC.Columns[9].Name = "WtMtr";
                DataGridQC.Columns[9].HeaderText = "Pk W/Mtr";
                DataGridQC.Columns[9].DataPropertyName = "PkWtMtr";

                DataGridQC.Columns[10].Name = "QcSts";
                DataGridQC.Columns[10].HeaderText = "QcSts";
                DataGridQC.Columns[10].DataPropertyName = "QcSts";

                DataGridQC.Columns[11].Name = "RefUid";
                DataGridQC.Columns[11].HeaderText = "RefUid";
                DataGridQC.Columns[11].DataPropertyName = "RefUid";
                DataGridQC.Columns[11].Visible = false;
                DataGridQC.DataSource = bsQC;
                CellValueChanged();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnRefersh_Click(object sender, EventArgs e)
        {
            try
            {
                GetQC(0);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void cmbQCSt_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cmbQCSt.SelectedIndex != -1)
                {
                    GetQC(0);
                    if (cmbQCSt.Text == "Mending")
                    {
                        btnMending.Enabled = true;
                    }
                }
                CellValueChanged();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
        protected void CellValueChanged()
        {
            try
            {
                int i = 0;
                foreach (DataGridViewRow row in DataGridQC.Rows)
                {
                    if (row.Cells[10].Value.ToString() == "QC Pass")
                    {
                        row.DefaultCellStyle.BackColor = Color.LightGreen;
                    }
                    else
                    {
                        row.DefaultCellStyle.BackColor = Color.Yellow;
                    }
                    row.Cells[1].Value = i + 1;
                    i++;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                bsQC.Filter = string.Format("QCBarcode Like '%{0}%' or SortNo Like '%{1}%' or MentorName Like '%{2}%'", txtSearch.Text, txtSearch.Text, txtSearch.Text);
                CellValueChanged();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnMending_Click(object sender, EventArgs e)
        {
            try
            {
                tag = "Mending";
                if (tag == "Mending")
                {
                    LoadTitle();
                    dtBarcode = db.GetData(CommandType.StoredProcedure, "SP_GetQCBarcode");
                    int Index = DataGridQC.SelectedCells[0].RowIndex;
                    string Barcode = DataGridQC.Rows[Index].Cells[4].Value.ToString();
                    grFront.Visible = false;
                    grCutpice.Visible = true;
                    SqlParameter[] para = {
                        new SqlParameter("@QCBarcode",Barcode)
                    };
                    DataSet ds = db.GetMultipleData(CommandType.StoredProcedure, "SP_GetQCMending", para);
                    DataTable dtQcDet = ds.Tables[0];
                    DataTable dtQcM = ds.Tables[1];
                    DataTable dtSort = ds.Tables[2];
                    txtbarcode.Text = Barcode;
                    txtbarcode.Tag = dtQcDet.Rows[0]["QcDUid"].ToString();
                    cmbNoofPices.SelectedIndex = 0;
                    lblMendorName.Text = dtQcM.Rows[0]["MentorName"].ToString();
                    QCDuid = Convert.ToInt32(dtQcDet.Rows[0]["QCDuid"].ToString());
                    lblSortNo.Text = dtSort.Rows[0]["SortNo"].ToString();
                    lblWtPMtr.Text = dtSort.Rows[0]["PwPm"].ToString();
                    if (lblWtPMtr.Text == "0.000")
                    {
                        lblWtPMtr.Text = dtSort.Rows[0]["WtMtr"].ToString();
                    }
                    txtbarcode.Tag = dtSort.Rows[0]["id"].ToString();
                    txtMeter.Tag = dtSort.Rows[0]["GTLEN"].ToString();
                    txtWeight.Tag = dtQcDet.Rows[0]["Weight"].ToString();
                    cmbNoofPices.SelectedIndex = 0;
                    decimal totalWght = Convert.ToDecimal(dtQcM.Rows[0]["TotalWeight"].ToString());
                    decimal Weight = Convert.ToDecimal(Convert.ToDecimal(dtQcDet.Rows[0]["Weight"].ToString()));
                    txtNetWeight.Text = "0";
                    cmbNoofPices.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void dtpfilterDate_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                cmbQCSt_SelectedIndexChanged(sender, e);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult res = MessageBox.Show("Do you want delete this data?", "Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res == DialogResult.Yes)
                {
                    int Index = DataGridQC.SelectedCells[0].RowIndex;
                    SqlParameter[] para = {
                        new SqlParameter("@QcUid",DataGridQC.Rows[Index].Cells[0].Value),
                        new SqlParameter("@Message",SqlDbType.NVarChar,50),
                        new SqlParameter("@QCBarcode",DataGridQC.Rows[Index].Cells[4].Value),   
                    };
                    para[1].Direction = ParameterDirection.Output;
                    string Meaage = db.ExecuteQueryString(CommandType.StoredProcedure, "SP_QcDelete", para, 1);
                    if (Meaage == "Deleted")
                    {
                        MessageBox.Show("Deleted Sucessfully", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        GetQC(0);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("Already Packed", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void btnPrintBarCode_Click(object sender, EventArgs e)
        {
            try
            {
                dtBarcode = db.GetData(CommandType.StoredProcedure, "SP_GetQCBarcode");
                int Index = DataGridQC.SelectedCells[0].RowIndex;
                int QcUid = Convert.ToInt32(DataGridQC.Rows[Index].Cells[0].Value.ToString());
                string QcBarcode = DataGridQC.Rows[Index].Cells[4].Value.ToString();
                SqlParameter[] parameters = { new SqlParameter("@QcUid", QcUid), new SqlParameter("@QCBarcode", QcBarcode) };
                DataTable dt = db.GetData(CommandType.StoredProcedure, "SP_QCPrintBarcode", parameters);
                if (dt.Rows.Count != 0)
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        FileStream fs = new FileStream(Application.StartupPath + "\\QCBarcode.txt", FileMode.Truncate, FileAccess.Write);
                        fs.Close();
                        for (int i = 0; i < dtBarcode.Rows.Count; i++)
                        {
                            string Var = dtBarcode.Rows[i]["VarType"].ToString();
                            if (Var == "Static")
                            {
                                File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", dtBarcode.Rows[i]["BCodeFormat"].ToString() + Environment.NewLine);
                            }
                            else
                            {
                                int LineNUmber = Convert.ToInt32(dtBarcode.Rows[i]["LineNumber"].ToString());
                                if (LineNUmber == 10)
                                {
                                    string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[j]["QCBarcode"].ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 12)
                                {
                                    string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[j]["QCBarcode"].ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 19)
                                {
                                    string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[j]["SortNo"].ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 20)
                                {
                                    string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(dt.Rows[j]["Meters"].ToString()).ToString("0.000") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 21)
                                {
                                    string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDecimal(dt.Rows[j]["Weight"].ToString()).ToString("0.000") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 24)
                                {
                                    string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + Convert.ToDateTime(dt.Rows[j]["QCDate"].ToString()).ToString("dd.MM.yyyy") + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                                }
                                else if (LineNUmber == 27)
                                {
                                    string text = dtBarcode.Rows[i]["BCodeFormat"].ToString() + "\"" + dt.Rows[j]["MentorName"].ToString() + "\"";
                                    File.AppendAllText(Application.StartupPath + "\\QCBarcode.txt", text + Environment.NewLine);
                                }
                            }
                        }
                        Process.Start(Application.StartupPath + "\\BPQc.bat");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
        }
    }
}
